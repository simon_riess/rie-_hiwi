function [lanes, traject, starts] = xml_file_read(filename,isMap)
    %XML_FILE_READ converts an xml file which contains lane borders
    %params:
    %   filename    path to the file
    %   isMap       logical, true if file is a Map - false if file contains
    %               trajectories for dynamic obstacles
    %return:
    %   lanes       if isMap = true: array of lanes (ordered lane borders
    %                   left and right)
    %               if isMap = false: array of all ways
    %   traject     array of trajectories if existing

%reading the Xml file in Matlab%
xdoc = xmlread(filename);

%Extracting the nodes from Xml file%
ne = xdoc.getElementsByTagName('node');
a = [];
for k = 0:ne.getLength-1
    a(:,end+1) = [ str2double(ne.item(k).getAttribute('id')); ...
        str2double(ne.item(k).getAttribute('lat')); ...
        str2double(ne.item(k).getAttribute('lon')) ];
end

%Extracting the ways from the Xml file%
n1 = xdoc.getElementsByTagName('way');
ways = {};
traj = {};
for k = 0:n1.getLength()-1
    disp('-------')
    disp(['way: ' char(n1.item(k).getAttribute('id'))])
    n2 = n1.item(k).getElementsByTagName('nd');
    way_ind = [];
    for i=0:n2.getLength()-1
        disp(char(n2.item(i).getAttribute('ref')))
        ind = find(a(1,:)==str2double(n2.item(i).getAttribute('ref')));
        way_ind(:, end+1) = a(2:3, ind);
    end
    ways{1,k+1} = way_ind;
    %save ID to every lane
    ways{2,k+1} = str2double(n1.item(k).getAttribute('id'));
    
    %get trajectories
    tag = n1.item(k).getElementsByTagName('tag')
    if ~isempty(tag.item(0))
        v = tag.item(0).getAttribute('v');
        if strcmp(v, 'trajectory')
            traj{1,end+1} = way_ind;
            
            if ~isempty(tag.item(1))
                vel = tag.item(1).getAttribute('k');
                if strcmp(vel, 'velocity')
                    v = tag.item(1).getAttribute('v');
                    traj{2,end} = str2double(v);
                end
            end
        end
    end
end

%convert trajectories
traject = traj;
for k = 1:size(traj,2)
    [x,y,utmzone] = deg2utm(traj{1,k}(1,:),traj{1,k}(2,:));
    traject{1,k} = [x';y'];
end

%if this file contains a map, check for relations (lanes) and 
%left/right border, else return empty array
if isMap
    %lanes = ways;
    ls = ways;
    %converting lat and lon from degrees to meters & plotting ways%
    for i = 1:n1.getLength()
        [x,y,utmzone] = deg2utm(ways{1,i}(1,:),ways{1,i}(2,:));
        %return converted lanes
        ls{1,i} = [x';y'];
    end

    %TODO: normalize this don't work
    %normalize x and y
    %{
    for k = 1:length(ls)
        if k == 1
            minX = min(ls{1,1}(1,:));
            minY = min(ls{1,1}(2,:));
        else
            minX2 = min(ls{1,k}(1,:));
            minY2 = min(ls{1,k}(2,:));
            if minX > minX2
                minX = minX2;
            end
            if minY > minY2
                minY = minY2;
            end
        end
    end
    for k = 1:length(ls)<way id="-242" action="modify" visible="true"><nd ref="-50"/><nd ref="-46"/><nd ref="-44"/><nd ref="-42"/><nd ref="-40"/><nd ref="-38"/><nd ref="-36"/><nd ref="-34"/><nd ref="-32"/><nd ref="-30"/><nd ref="-28"/><nd ref="-26"/><nd ref="-24"/><nd ref="-22"/><nd ref="-20"/><nd ref="-18"/><nd ref="-16"/><nd ref="-14"/><nd ref="-48"/><tag k="type" v="trajectory"/><tag k="velocity" v="10"/></way>
        ls{1,k}(1,:) = ls{1,k}(1,:)/minX;
        ls{1,k}(2,:) = ls{1,k}(2,:)/minY;
    end
    %}

    %Extracting the relations from Xml file%
    n3 = xdoc.getElementsByTagName('relation');
    tempLanes = cell(1,n3.getLength());
    tempStarts = cell(1, n3.getLength());
    lane = cell(1,2);
    point = [0,0];
    countLanes=0;
    countStarts=0;
    
    for k = 0:n3.getLength()-1
        disp('-------')
        disp(['relation: ' char(n3.item(k).getAttribute('id'))])
        n4 = n3.item(k).getElementsByTagName('member');
        
        tag = n3.item(k).getElementsByTagName('tag');
        v = tag.item(0).getAttribute('v');
        
        if strcmp(v,'lanelet')
            countLanes = countLanes + 1;
            for i=0:n4.getLength()-1
                disp('lanelet')
                disp(char(n4.item(i).getAttribute('ref')))
                %left == 0, right == 1
                %find lanes in relation, place according to left/right
                idx = find(cell2mat(ls(2,:))==str2double(n4.item(i).getAttribute('ref')));
                side = n4.item(i).getAttribute('role');
                if strcmp(side,'left')
                    lane{1} = ls{1,idx};
                else
                    lane{2} = ls{1,idx};
                end
            end
            tempLanes{countLanes} = lane;
        elseif strcmp(v, 'start')
            
            for i=0:n4.getLength()-1
                countStarts = countStarts + 1;
                disp('starting point')
                disp(char(n4.item(i).getAttribute('ref')))
                idx = find(a(1,:) == str2double(n4.item(i).getAttribute('ref')));
                [point(1),point(2),utmzone] = deg2utm(a(2,idx),a(3,idx));
                tempStarts{countStarts} = point;
            end
            
        end
            
    end
    
    lanes = cell(1, countLanes);
    for i=1:countLanes
        lanes{1,i} = tempLanes{1,i};
    end
    
    starts = cell(1, countStarts);
    for i=1:countStarts
        starts{1,i} = tempStarts{1,i};
        disp('Punkt vorhanden')
        disp(starts{1,i})
    end
    disp(size(starts))
else
    %return empty array
    lanes = [];
end