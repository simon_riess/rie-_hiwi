classdef CompositionLane < handle
   %COMPOSITIONLANE logical layer of combined lanelets for one car path
   %which describes one occupancy set
   
   properties (SetAccess = protected)
        laneIDs     %array of lane id's in order of driving
        minArcLen   %minimum arc length of prediction
        maxArcLen   %maximum arc length of prediction
        allLanesLen %length of general lane (with all lanes from laneIDs combined)
        precVeh     %index of preceding car in occupancyPrediction.cars array if it exists
        followVeh   %index of following car in occupancyPrediciton.cars array if it exists
        isAdapted   %logical false: original occupancy; true: adapted occupancy
   end
   
   methods
       function obj = CompositionLane(laneIDs, minArcLen, maxArcLen, allLanesLen, precVeh, followVeh)
           obj.laneIDs = laneIDs;
           obj.minArcLen = minArcLen;
           obj.maxArcLen = maxArcLen;
           obj.allLanesLen = allLanesLen;
           obj.precVeh = precVeh;
           obj.followVeh = followVeh;
           obj.isAdapted = false;
       end
       
       function addLaneID(obj, laneID)
          %ADDLANEID adds a lane id to the laneID array
          
          obj.laneIDs(end+1) = laneID;
       end
       
       function setPrecVeh(obj, precVeh)
           %SETPRECVEH set the property precVeh to |precVeh|
           
           obj.precVeh = precVeh;
       end
       
       function setFollowVeh(obj, followVeh)
           %SETFOLLOWVEH set the property followVeh to |followVeh|
           
           obj.followVeh = followVeh;
       end
       
       function setMinArcLen(obj, minArcLen)
          %SETMINARCLEN set the minArcLen to |minArcLen|
          
          obj.minArcLen = minArcLen;
       end
       
       function setMaxArcLen(obj, maxArcLen)
          %SETMAXARCLEN set the maxArcLen to |maxArcLen|
          
          obj.maxArcLen = maxArcLen;
       end
       
       function setIsAdapted(obj, adapted)
           %SETADAPTED set the isAdapted logical value to |adapted|
           
           obj.isAdapted = adapted;
       end
   end
end