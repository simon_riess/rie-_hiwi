classdef OccupancyPrediction < handle
    %OCCUPANCYPREDICTION Predicts the occupancy of other traffic participants
    %
    %   This class provides methods to estimate future occupancy of moving obstacles
    %   according to their dynamics and to traffic rules
    
    %         prediction 1       prediction 2
    %     |------------------|------------------|
    % time_point i     time_point i+1    time_point i+2
    
    properties (SetAccess = private, GetAccess = protected)
        perception
        c = [0.8 0.8 0.8]; % color
    end
    
    properties (SetAccess = protected)
        occupancy_set_polygon = {};     %form of occupancy area (predicted occupancy for all obstacles/cars)
        occupancy_set_rect = {};        %form of occupancy area (predicted occupancy for all obstacles/cars)
        occupancyForm                   %form of occupancy which should be calculated, Rect or Polygon
        n_prediction_time_points = 10;  %number of prediction time points
        timePoints = 0;                 %array containing time points from (1 to n_prediction_time_points) + c_t
        ts;                             %time step
        cars;                           %cars
    end
    
    properties (Constant)
        a_max = 5;              %acceleration maximum
        v_max = 30;             %velocity maximum
        v_s = 7;                %parameterized speed (=maximum engine power)
        CAR_LENGTH_HALF = 2.5;  %half of the car length
    end
    
    methods

        function obj = OccupancyPrediction(perception, ts, occupancyForm)
            obj.perception = perception;
            obj.reset_prediction_set();
            obj.occupancyForm = occupancyForm;
            obj.ts = ts;
            obj.cars = simulation.SimulatedCar.empty();
        end
        
        function set_prediction_horizon(obj, N)
            obj.n_prediction_time_points = N;
        end
        
        %function add_car(obj, car)
        %    obj.cars(end+1) = car;
        %end
        
        %'main function', predicts the occupancy of all traffic
        %participants (cars)
        function update_prediction(obj)
            obj.reset_prediction_set();
            
            c_t = obj.perception.getTime();
            obj.timePoints = (0:1:obj.n_prediction_time_points-1)+c_t;
            obj.cars = obj.perception.getDetectedCars();
            %all existing lanes
            lanelets = obj.perception.getLanes();
            
            %place cars on nearest lane
            for k = 1:length(obj.cars)
                [lane, lane_position_idx, residual] = obj.findNearestLane( ...
                obj.cars(k).position);
                obj.cars(k).laneID = lane.id;
                
                switch obj.occupancyForm
                    case collision_detection.CollisionObject.Rect
                        os = occupancy_prediction.Obstacle.create_empty_dynamic_obstacle(...
                            collision_detection.CollisionObject.Rect, obj.timePoints, obj.ts);
                    case collision_detection.CollisionObject.Polygon
                        os = occupancy_prediction.Obstacle.create_empty_dynamic_obstacle(...
                            collision_detection.CollisionObject.Polygon, obj.timePoints, obj.ts);
                    otherwise
                        error('[OccupancyPrediction - update-prediction] occupancyForm invalid');
                end
                
                obj.cars(k).obstacle = os;
                
                [pt_out, dist, road_distance, idx] = lane.closest_point_on_lane(obj.cars(k).position);
                obj.cars(k).roadDistance = road_distance;
            end
            
            %for every time point calculate occupancy
            it = 1;
            while it <= numel(obj.timePoints)
                switch obj.occupancyForm
                    case collision_detection.CollisionObject.Rect
                        %for every car in the scenario approximate
                        %occupancy
                        for k = 1:length(obj.cars)
                            obj.predict_single_approx(obj.cars(k), it, lanelets, obj.cars(k).roadDistance);                            
                        end
                        %if occupancy exceeds map in time step it - no
                        %occupancy is created and therefore no adaption and
                        %transformation to a rectangle is needed
                        if it <= length(obj.cars(k).compLanes)                            
                            %adapt occupancy if preceding vehicle exists and
                            %overlaps
                            obj.adaptOccOfPrecVehAtTime(it, lanelets);
                            %create rectangle
                            for k = 1:length(obj.cars)                                
                                for j = 1:length(obj.cars(k).compLanes{it})
                                    obj.createRectFromCompLane(obj.cars(k).compLanes{it}{j}, it, lanelets, obj.cars(k));
                                end
                            end
                        end
                    case collision_detection.CollisionObject.Polygon
                        mapExceeding = false;
                        %for every car in the scenario approximate
                        %occupancy
                        for k = 1:length(obj.cars)
                            obj.predict_single_approx(obj.cars(k), it, lanelets,obj.cars(k).roadDistance);
                            if it > length(obj.cars(k).compLanes)
                                mapExceeding = true;
                            else
                                for l = 1:length(obj.cars(k).compLanes{it})
                                    if isempty(obj.cars(k).compLanes{it}{l})
                                        mapExceeding = true;
                                        break;
                                    end
                                end
                            end
                        end 
                        %if occupancy exceeds map in time step it - no
                        %occupancy is created and therefore no adaption and
                        %transformation to a polygon is needed                                
                        if ~mapExceeding
                            %adapt occupancy if preceding vehicle exists and
                            %overlaps
                            obj.adaptOccOfPrecVehAtTime(it, lanelets);
                            %create polygon
                            for k = 1:length(obj.cars)                              
                                for j = 1:length(obj.cars(k).compLanes{it})
                                    obj.createPolygonFromCompLane(obj.cars(k).compLanes{it}{j}, it, lanelets, obj.cars(k));
                                end                                
                            end  
                        end
                    otherwise
                        error('[OccupancyPrediction - update_prediction] occupancyForm invalid');
                end
                it = it+1;
            end
        end
        
        % create an 'occupancy_prediction.obstacle' object, described by an
        % composition lane
        function predict_single_approx(obj, obstacle, it, lanelets, road_distance)
        %PREDICT_SINGLE_APPROX predicts the occupancy of the obstacle in
        %time step |it| and creates therefore a composition lane object
        %   obstacle    -   obstacle for which the occupancy is predicted
        %                   (usually a car)
        %   it          -   index of current time step in |time_points|
        %   lanelets    -   array of all existing lanes
        %   road_distance - road_distance from the beginning of lane where
        %                   ostacle is placed to position of obstacle
        
            v_i = obstacle.velocity;
            c_t = obj.perception.getTime();
            
            % !!! MODIFIED TO USE IT FOR REACHABILITY SAMPLING
            %c_t = 0;
            % !!! REMOVE
                        
            %calculate max/min position of obstacle
            
            s_max = occupancy_prediction.OccupancyPrediction. ...
                farthestLongitudinalReachability(v_i, obj.a_max, obj.v_max, ...
                obj.v_s, (obj.timePoints(it)+1-c_t)*obj.ts);
            s_min = occupancy_prediction.OccupancyPrediction. ...
                closestLongitudinalReachability(v_i, obj.a_max, ...
                (obj.timePoints(it)-c_t)*obj.ts);

            
            %for all lanes calculate composition lanes
            minArcLen = road_distance + s_min - obj.CAR_LENGTH_HALF;
            maxArcLen = road_distance + s_max + obj.CAR_LENGTH_HALF;
            if it == 1
                %get lane with laneID
                lane = obj.getLaneWithID(obstacle.laneID, lanelets);
                %get length of lane
                laneLen = lane.distance(end);
                if laneLen > maxArcLen
                    %create new composition lane on same lanelet
                    cl = occupancy_prediction.CompositionLane(obstacle.laneID, ...
                        minArcLen, maxArcLen, laneLen, [], []);
                    %add
                    obstacle.compLanes{1}{1} = cl;
                else
                    %create new composition lane with adjacent lanelets if
                    %existing
                    if ~isempty(lane.adjacentLanes)
                        laneAdjLs = lane.adjacentLanes;
                        %add all adjacentLanes
                        for n = 1:length(laneAdjLs)
                            adjLaneId = laneAdjLs(n);
                            for j = 1:length(lanelets)
                              if adjLaneId == lanelets{1,j}.id
                                sumLaneLen = laneLen + lanelets{1,j}.distance(end);
                                cl = occupancy_prediction.CompositionLane([obstacle.laneID, adjLaneId],...
                                    minArcLen, maxArcLen, sumLaneLen, [], []);
                                obstacle.compLanes{1}{n} = cl;
                                break;
                              end
                            end
                        end
                    else
                        msg = '[OccupancyPrediction] predict_single_approx: occupancy exceeds map in time step: 1';
                        msg = strcat(msg, ', for example due to a too large prediction horizon. No prediction possible');
                        warning(msg);
                        obj.timePoints = 1;
                    end
                end
            else
                for k = 1:length(obstacle.compLanes{it-1})
                    if obstacle.compLanes{it-1}{k}.allLanesLen > maxArcLen
                        %create new composition lane on same lanelet
                        cl = occupancy_prediction.CompositionLane(obstacle.compLanes{it-1}{k}.laneIDs, ...
                            minArcLen, maxArcLen, obstacle.compLanes{it-1}{k}.allLanesLen, ...
                            obstacle.compLanes{it-1}{k}.precVeh, obstacle.compLanes{it-1}{k}.followVeh);
                        obstacle.compLanes{it}{k} = cl;
                    else
                        %create new compostion lane with adjacent lanelets
                        %if existing
                        lastLaneId = obstacle.compLanes{it-1}{k}.laneIDs(end);
                        for j = 1:length(lanelets)
                            if lastLaneId == lanelets{1,j}.id
                                adjLanes = lanelets{1,j}.adjacentLanes;
                                break;
                            end
                        end
                     
                        if ~isempty(adjLanes)                    
                            %add all adjacentLanes
                            for n = 1:length(adjLanes)
                                adjLaneId = adjLanes(n);
                                for j = 1:length(lanelets)
                                  if adjLaneId == lanelets{1,j}.id
                                    sumLaneLen = obstacle.compLanes{it-1}{k}.allLanesLen + lanelets{1,j}.distance(end);
                                    cl = occupancy_prediction.CompositionLane([obstacle.compLanes{it-1}{k}.laneIDs, adjLaneId],...
                                        minArcLen, maxArcLen, sumLaneLen, obstacle.compLanes{it-1}{k}.precVeh, ...
                                        obstacle.compLanes{it-1}{k}.precVeh);
                                    if k == 1
                                        obstacle.compLanes{it}{n} = cl;
                                    else
                                        obstacle.compLanes{it}{end+1} = cl;
                                    end
                                    break;
                                  end
                                end
                            end
                        else
                            msg = '[OccupancyPrediction] predict_single_approx: occupancy exceeds map in time step: ';
                            msg = strcat(msg, num2str(it));
                            msg = strcat(msg, ', for example due to a too large prediction horizon. Prediction of occupancy sets only done until time step: ');
                            msg = strcat(msg, num2str(it-1));
                            warning(msg);
                            obj.timePoints = (1:it);
                        end
                    end
               end
            end
        end
        
        function createRectFromCompLane(obj, compLane, it, lanelets, car)
           %CREATERECTFROMCOMPLANE creates a rectangle from a compostition 
           %lane
           %params:
           %   compLane    -   composition lane from which a rectangle is
           %                   created
           %   it          -   index of current time step in |time_points|
           %   lanelets    -   array of all existing lanes
           %   car         -   car for which rectangle is predicted
           
           if ~isempty(compLane)
                lastLane = obj.getLaneWithID(compLane.laneIDs(end), lanelets);
                lenOfAllLanesButLast = compLane.allLanesLen - lastLane.distance(end);
                if (compLane.minArcLen - lenOfAllLanesButLast) > 0
                    %occupancy just on last lanelet
                    minLen = compLane.minArcLen - lenOfAllLanesButLast;
                    maxLen = compLane.maxArcLen - lenOfAllLanesButLast;
                    [pt_min, ptMinRight, ptMinLeft, idxMin] = lastLane.pos_interp(minLen);
                    [pt_max, ptMaxRight, ptMaxLeft, idxMax] = lastLane.pos_interp(maxLen);
                    %plot min and max border - for step-based test purposes
                    %hold on;
                    %plot_point(pt_min, 'r+');
                    %plot_point(pt_max, 'b*');
                    
                    %create rectangle
                    tmp = pt_max-pt_min;
                    len = norm(tmp);
                    orientation = atan2(tmp(2), tmp(1));
                    rect = collision_detection.Rectangle(lastLane.width, ...
                        len,orientation, 0.5*(pt_max+pt_min));
                    %save calculated rectangle in
                    %obstacle.geometric_boundary
                    car.obstacle.set_geometric_boundary_at_time(it, rect);
                    %save calculated rectangle in
                    %occupancyPrediction.occupancy
                    obj.addRectApproxAtTime(it, rect);
                else
                    %occupancy over several lanelets
                    diff = 0;
                    k = 0;
                    lanes{1} = lastLane;
                    lenOfAllLanesButLasts = lenOfAllLanesButLast;
                    while diff <= 0
                        k = k+1;
                        lanes{end+1} = obj.getLaneWithID(compLane.laneIDs(end-k), lanelets);             
                        lenOfAllLanesButLasts = lenOfAllLanesButLasts - lanes{end}.distance(end);
                        diff = compLane.minArcLen - lenOfAllLanesButLasts;
                    end
                    [pt_min, ptMinRight, ptMinLeft, idxMin] = lanes{end}.pos_interp(compLane.minArcLen-lenOfAllLanesButLasts);
                    [pt_max, ptMaxRight, ptMaxLeft, idxMax] = lanes{1}.pos_interp(compLane.maxArcLen-lenOfAllLanesButLast);
                    %plot min and max border - for step-based test purposes
                    %hold on;
                    %plot_point(pt_min, 'r+');
                    %plot_point(pt_max, 'b*');
                    
                    %create rectangle
                    tmp = pt_max-pt_min;
                    len = norm(tmp);
                    orientation = atan2(tmp(2), tmp(1));
                    if lanes{end}.width > lanes{1}.width
                        w = lanes{end}.width;
                    else
                        w = lanes{1}.width;
                    end
                    rect = collision_detection.Rectangle(w, ...
                        len,orientation, 0.5*(pt_max+pt_min));
                    %save calculated rectangle in
                    %obstacle.geometric_boundary
                    car.obstacle.set_geometric_boundary_at_time(it, rect);
                    %save calculated rectangle in
                    %occupancyPrediction.occupancy
                    obj.addRectApproxAtTime(it, rect);
                end
            else
               error('[OccupancyPrediction] createRectFromCompLane : param |compLane| empty - no creation possible. Possible cause: maximum occupancy border exceeds lane length');              
            end
        end
        
        function createPolygonFromCompLane(obj, compLane, it, lanelets, car)
            %CREATEPOLYGONFROMCOMPLANE creates a polygon from a composition
            %lane
            %params:
            %   compLane    -   composition lane from which a poylgon is
            %                   created
            %   it          -   index of current time step in |time_points|
            %   lanelets    -   array of all existing lanes
            %   car         -   car for which polygon is predicted
            
            if ~isempty(compLane)
                lastLane = obj.getLaneWithID(compLane.laneIDs(end), lanelets);
                lenOfAllLanesButLast = compLane.allLanesLen - lastLane.distance(end);
                %calculate polygon
                if (compLane.minArcLen - lenOfAllLanesButLast) > 0
                    %occupancy just on last lanelet
                    minLen = compLane.minArcLen - lenOfAllLanesButLast;
                    maxLen = compLane.maxArcLen - lenOfAllLanesButLast;
                    [pt_min, ptMinRight, ptMinLeft, idxMin] = lastLane.pos_interp(minLen);
                    [pt_max, ptMaxRight, ptMaxLeft, idxMax] = lastLane.pos_interp(maxLen);
                    %plot min and max border - for step-based test purposes
                    %hold on;
                    %plot_point(pt_min, 'r+');
                    %plot_point(pt_max, 'b*');
                    
                    %create polygon
                    %add min border
                    polygon = collision_detection.Polygon([ptMinLeft, pt_min, ptMinRight]);
                    %get points between min and max border = right border
                    for index = idxMin+1:idxMax
                        polygon.addVertice(lastLane.right_border(:,index));
                    end
                    %add max border
                    polygon.addVertices([ptMaxRight, pt_max, ptMaxLeft]); 
                    %get points between max and min border = left border
                    for index = idxMax:-1:idxMin+1
                        polygon.addVertice(lastLane.left_border(:,index));
                    end
                    %save calculated polygon in
                    %obstacle.geometric_boundary
                    car.obstacle.set_geometric_boundary_at_time(it, polygon);
                    %save calculated polygon in
                    %occupancyPrediction.occupancy
                    obj.addPolygonApproxAtTime(it, polygon);
                else
                    %occupancy over several lanelets
                    diff = 0;
                    k = 0;
                    lanes{1} = lastLane;
                    lenOfAllLanesButLasts = lenOfAllLanesButLast;
                    while diff <= 0
                        k = k+1;
                        lanes{end+1} = obj.getLaneWithID(compLane.laneIDs(end-k), lanelets);
                        lenOfAllLanesButLasts = lenOfAllLanesButLasts - lanes{end}.distance(end);
                        diff = compLane.minArcLen - lenOfAllLanesButLasts;
                    end
                    [pt_min, ptMinRight, ptMinLeft, idxMin] = lanes{end}.pos_interp(compLane.minArcLen-lenOfAllLanesButLasts);
                    [pt_max, ptMaxRight, ptMaxLeft, idxMax] = lanes{1}.pos_interp(compLane.maxArcLen-lenOfAllLanesButLast);
                    %plot min and max border - for step-based test purposes
                    %hold on;
                    %plot_point(pt_min, 'r+');
                    %plot_point(pt_max, 'b*');
                    
                    %create polygon
                    %add min border
                    polygon = collision_detection.Polygon([ptMinLeft, pt_min, ptMinRight]);
                    %get points between min and max border = right border
                    for index = idxMin+1:length(lanes{end}.right_border) 
                        polygon.addVertice(lanes{end}.right_border(:,index));
                    end
                    for k = 1:length(lanes)-2
                        for index = 1:length(lanes{end-k}.right_border) 
                            polygon.addVertice(lanes{end-k}.right_border(:,index));
                        end
                    end              
                    for index = 1:idxMax
                        polygon.addVertice(lanes{1}.right_border(:,index));
                    end
                    %add max border
                    polygon.addVertices([ptMaxRight, pt_max, ptMaxLeft]);               
                    %get points between max and min border = left border
                    for index = idxMax:-1:1 
                        polygon.addVertice(lanes{1}.left_border(:,index));
                    end
                    for k = 2:length(lanes)-1
                        for index = length(lanes{k}.left_border):-1:1
                            polygon.addVertice(lanes{k}.left_border(:,index));
                        end
                    end
                    for index = length(lanes{end}.left_border):-1:idxMin+1
                        polygon.addVertice(lanes{end}.left_border(:,index));
                    end
                    %save calculated polygon in
                    %obstacle.geometric_boundary
                    car.obstacle.set_geometric_boundary_at_time(it, polygon);
                    %save calculated polygon in
                    %occupancyPrediction.occupancy
                    obj.addPolygonApproxAtTime(it, polygon);
                end
            else
               error('[OccupancyPrediction] createPolygonFromCompLane : param |compLane| empty - no creation possible. Possible cause: maximum occupancy border exceeds lane length');              
            end
        end
        
        function adaptOccOfPrecVehAtTime(obj, it, lanelets)
            %ADAPTOCCOFPRECVEHATTIME checks for preceding vehicles and checks for
            %overlapping occupancy sets for time step it
            %params:
            %   obj     -   occupancyPrediction object
            %   it      -   index of current time step in |time_points|
            %   lanelets-   array of all existing lanes
            
            %for all compositionLanes for time step |it|
            for k = 1:length(obj.cars)
                compLnsCar1 = obj.cars(k).compLanes{it};
                for n = 1:length(compLnsCar1)
                    %if compositionLane wasn't already adapted
                    if ~compLnsCar1{1,n}.isAdapted
                        %check for preceding cars and adapt occupancy
                        obj.checkForOverlapsAtTime(it, compLnsCar1{1,n}, k, n, lanelets);
                    end
                end
            end
        end
        
        function checkForOverlapsAtTime(obj, it, compLane, k, n, lanelets)
            %CHECKFOROVERLAPSATTIME identifies overlapsing occupancy with
            %occupancy of |compLane| for time step it and adapts occupancy
            %params:
            %   it          -   index of current time step in |time_points|
            %   compLane    -   compositionLane for which overlaps has to
            %                   be checked
            %   k           -   index of car which corresponds to compLane
            %   n           -   index of compLane in compLane array
            %   lanelets    -   array of all existing lanes
            
            %if no preceding vehicle is known, search for one, else adapt the
            %occupancy recursively from the start of a car chain to the end
            if isempty(compLane.precVeh)
                %search for preceding vehicle
                obj.findPrecedingCar(it, compLane, k, n, lanelets);
            else
                precVehCompLns = obj.cars(compLane.precVeh).compLanes{it};
                for l = 1:length(precVehCompLns)
                    if ~precVehCompLns{1,l}.isAdapted
                        obj.checkForOverlapsAtTime(it, precVehCompLns{1,l}, compLane.precVeh, l, lanelets);
                    end
                end
                obj.adaptOccupancyAtTime(it, n, k, compLane.precVeh, lanelets);
            end
        end
        
        function findPrecedingCar(obj, it, compLane, k, n, lanelets)
           %FINDPRECEDINGCAR finds preceding car of car k corresponding to
           %compLane for time step it
           %params:
           %   it          -   index of current time step in |time_points|
           %   compLane    -   compositionLane for which overlaps has to
           %                   be checked
           %   k           -   index of car which corresponds to compLane
           %   n           -   index of compLane in compLane array
           %   lanelets    -   array of all existing lanes
           
           lastLnIdCar1 = compLane.laneIDs(end);
           for j = 1:length(obj.cars)
               if j ~= k
                   compLnsCar2 = obj.cars(j).compLanes{it};
                   for o = 1:length(compLnsCar2)
                       %if car2 has a following vehicle don't test it
                       if ~isempty(compLnsCar2{1,o}.followVeh)
                           break;
                       %if car2 has preceding vehicle which is car1
                       elseif ~isempty(compLnsCar2{1,o}.precVeh) && compLnsCar2{1,o}.precVeh == k
                           break;
                       else
                           %if the occupancies are on the same lanelet(s),
                           %both cars drive on the same lane
                           if any(compLnsCar2{1,o}.laneIDs == lastLnIdCar1)
                               %check which car is the preceding
                               idx = find(compLnsCar2{1,o}.laneIDs == lastLnIdCar1);
                               if idx == 1
                                   if length(compLane.laneIDs) == 1
                                       if compLane.minArcLen < compLnsCar2{1,o}.minArcLen
                                           %j is previous to k
                                           compLane.setPrecVeh(j);
                                           compLnsCar2{1,o}.setFollowVeh(k);
                                           %check for overlaps with this
                                           %new preceding vehicle (include
                                           %to check preceding vehicles of
                                           %this preceding vehicle)
                                           obj.checkForOverlapsAtTime(it, compLane, k, n, lanelets);
                                           continue;
                                       end
                                   else
                                       %j is previous to k
                                       compLane.setPrecVeh(j);
                                       compLnsCar2{1,o}.setFollowVeh(k);
                                       %check for overlaps with this
                                       %new preceding vehicle (include
                                       %to check preceding vehicles of
                                       %this preceding vehicle)
                                       obj.checkForOverlapsAtTime(it, compLane, k, n, lanelets);
                                       continue;
                                   end
                               end
                           end
                           %if no preceding vehicle set occupancy to
                           %isAdapted as there is no vehicle in front of it
                           %and no further adaption necessary
                           compLane.setIsAdapted(true);
                       end
                   end
               end
           end  
        end
        
        function adaptOccupancyAtTime(obj, it, n, carIdx, precVehIdx, lanelets)
            %ADAPTOCCUPANCYATTIME checks if there exists a overlap of
            %occupancy at time step it of |car| with preceding car
            %|precVeh|
            %params:
            %   it          -   index of current time step in |time_points|
            %   n           -   index of compLane for which ovelap is
            %                   checked
            %   carIdx      -   index for car object in
            %                   occupancyPrediction.cars
            %   precVehIdx  -   index of preceding car to |car| in
            %                   occupancyPrediction.cars
            %   lanelets    -   array of all existing lanes
            
            compLnCar = obj.cars(carIdx).compLanes{it}{1,n};
            compLnsPrecVeh = obj.cars(precVehIdx).compLanes{it};
            
            %trim length of min/max arc length of compLnCar in order to have 
            %the beginning of the length on same lanelet as
            %compLnsPrecVeh{1,k}
            for k = 1:length(compLnsPrecVeh)
                firstLnIdPrecVeh = compLnsPrecVeh{1,k}.laneIDs(1,1);
                %find index of this lanelet in compLnCar.laneIDs
                idx = find(compLnCar.laneIDs == firstLnIdPrecVeh);
                %trim maxArcLen in order to start from lanelet with id
                %|firstLnIdPrecVeh|
                maxLenCar = compLnCar.maxArcLen;
                diffLenCar = 0;
                for j = 1:idx-1
                    lane = obj.getLaneWithID(compLnCar.laneIDs(1,j), lanelets);
                    diffLenCar = diffLenCar + lane.distance(end);
                end
                maxLenCar = maxLenCar - diffLenCar;
                %check if it overlaps (maxLenCar larger than maxArcLen of
                %precVeh)
                if maxLenCar > compLnsPrecVeh{1,k}.maxArcLen - (obj.CAR_LENGTH_HALF*2)
                    %cut maxBorder of occupancy
                    newMaxLenCar = compLnsPrecVeh{1,k}.maxArcLen - ...
                        (obj.CAR_LENGTH_HALF*2) + diffLenCar;
                    obj.cars(carIdx).compLanes{it}{1,n}.setMaxArcLen(newMaxLenCar);
                    
                    %check if occupancy is large enough to contain car, if
                    %not -> crash of vehicles/accident -> adjust minBorder
                    if newMaxLenCar - compLnCar.minArcLen < (obj.CAR_LENGTH_HALF*2)                       
                        %set minBorder such that new occupancy length = car
                        %length of car
                        newMinLenCar = newMaxLenCar - (obj.CAR_LENGTH_HALF*2);

                        %if original minBorder was before maxBorder of precVeh,
                        %minBorderCar = min(newMinBorderCar, minBorderPrecVeh)
                        if newMaxLenCar < compLnCar.minArcLen
                            adaptedMinLenCar = newMinLenCar - diffLenCar;
                            if adaptedMinLenCar > compLnsPrecVeh{1,k}.minArcLen
                                newMinLenCar = compLnsPrecVeh{1,k}.minArcLen + diffLenCar;
                            end
                        end  
                        obj.cars(carIdx).compLanes{it}{1,n}.setMinArcLen(newMinLenCar);
                    end
                end
                %set adapted to true of preceding vehicle
                if ~compLnsPrecVeh{1,k}.isAdapted
                    compLnsPrecVeh{1,k}.setIsAdapted(true);
                end
            end            
            %set adapted to true of car
            compLnCar.setIsAdapted(true);            
        end
        
        % this is in order to add obstacles separately that are not stored
        % in "perception"
        function add_rect_approx(obj, obstacle)
        %ADD_RECT_APPROX adds a single obstacle to the
        %|occupancy_set_rect|
        %params:
        %   obstacle - obstacle which is added
            if numel(obj.occupancy_set_rect) == 0
                obj.occupancy_set_rect{1} = obstacle;
            else
                obj.occupancy_set_rect{end+1} = obstacle;
            end
        end
        
        function add_polygon_approx(obj, obstacle)
            %ADD_POLYGON_APPROX adds a single obstacle to
            %the |occupancy_set_polygon|
            %params:
            %   obstacle - obstacle which is added
            if numel(obj.occupancy_set_polygon) == 0
                obj.occupancy_set_polygon{1} = obstacle;
            else
                obj.occupancy_set_polygon{end+1} = obstacle;
            end
        end
        
        function addRectApproxAtTime(obj, time, rect)
           %ADDRECTAPPROXATTIME adds a rectangle at index |time| to the 
           %occupancy set
           %params:
           %   time    -   index of current time step in |time_points|
           %   rect    -   rectangle which is added at |time|
           
           if numel(obj.occupancy_set_rect) == 0
                obj.occupancy_set_rect{1}{1} = rect;
            elseif numel(obj.occupancy_set_rect) < time
                obj.occupancy_set_rect{time}{1} = rect;
            else
                obj.occupancy_set_rect{time}{end+1} = rect;
            end
        end
        
        function addPolygonApproxAtTime(obj, time, polygon)
            %ADDPOLYGONAPPROXATTIME adds a polygon at index |time| to the
            %occupanc set
            %params:
            %   time    -   index of current time step in |time_points|
            %   polygon -   polygon which is added at |time|
            
            if numel(obj.occupancy_set_polygon) == 0
                obj.occupancy_set_polygon{1}{1} = polygon;
            elseif numel(obj.occupancy_set_polygon) < time
                obj.occupancy_set_polygon{time}{1} = polygon;
            else
                obj.occupancy_set_polygon{time}{end+1} = polygon;
            end
        end
        
        % clear all predicted sets
        function reset_prediction_set(obj)
            obj.occupancy_set_polygon = {};
            obj.occupancy_set_rect = {};
        end
        
        function [lane, lane_position_idx, residual] = findNearestLane(obj, pos)
            % FINDNEARESTLANE searches the closest point of a lane to pos and returns this lane, the index of the center vertex
            % and the distance. This function does not necessarily find the closest lane since it only searches
            % the center vertices and not the line-segments
            
            residual = 10e6;
            lane = [];
            lane_position_idx = -1;
            for i=obj.perception.getLanes()
                idx = dsearchn(i{1}.center_vertices', pos');
                dist = norm(pos-i{1}.center_vertices(:,idx));
                if dist<residual
                    residual = dist;
                    lane = i{1};
                    lane_position_idx = idx;
                end
            end
        end
        
        function oc = getOccupancy(obj)
            %GETOCCUPANCY returns a matrix of obstacles of a matrix of
            %occupancies
            
            switch obj.occupancyForm
                case collision_detection.CollisionObject.Rect
                    oc = obj.occupancy_set_rect;
                case collision_detection.CollisionObject.Polygon
                    oc = obj.occupancy_set_polygon;
                otherwise
                    error('[OccupancyPrediction - getOccupancy] occupancyForm invalid');
            end
        end
        
        function setColor(obj, c)
            obj.c = c;
        end
        
        % DEPR
        %   function sync_time(obj, time_idx)
        %       obj.time_idx = time_idx;
        %   end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function plot3(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw3D(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw3D(ax);
            end
        end
        
        function draw(obj, axes_handle)
            hold on;
            switch obj.occupancyForm
                case collision_detection.CollisionObject.Rect
                    for k = 1:length(obj.occupancy_set_rect)
                        for j = 1:length(obj.occupancy_set_rect{1,k})
                            plot(obj.occupancy_set_rect{1,k}{1,j});
                        end
                    end
                case collision_detection.CollisionObject.Polygon
                    for k = 1:length(obj.occupancy_set_polygon)
                        %save initial figure                     
                        if simulation.GlobalPlotProperties.PRINT_FIGURE && k == 1
                            print(strcat('figures/simulation_oneLane_0'),'-depsc','-painters','-loose');
                        end                     
                        for j = 1:length(obj.occupancy_set_polygon{1,k})
                            plot(obj.occupancy_set_polygon{1,k}{1,j});
                        end
                        %save figure
                        if simulation.GlobalPlotProperties.PRINT_FIGURE
                            print(strcat('figures/simulation_oneLane_',num2str(k)),'-depsc','-painters','-loose');
                        end
                    end
                otherwise
                    error('[OccupancyPrediction - draw] occupancyForm invalid');
            end
            hold off;
        end
        
        function draw_time_interval(obj, axes_handle, t_start, t_stop)
            hold on;
            switch obj.occupancyForm
                case collision_detection.CollisionObject.Rect
                    for os = obj.occupancy_set_rect
                        os{1}.draw_time_interval(axes_handle, t_start, t_stop);
                    end
                case collision_detection.CollisionObject.Polygon
                    for os = obj.occupancy_set_polygon
                        os{1}.draw_time_interval(axes_handle, t_start, t_stop);
                    end
                otherwise
                    error('[OccupancyPrediction - draw_time_interval] occupancyForm invalid');
            end
            hold off;
        end
        
        function draw3D(obj, axes_handle)
            hold on;
            switch obj.occupancyForm
                case collision_detection.CollisionObject.Rect
                    for os = obj.occupancy_set_rect
                        os{1}.draw3D(axes_handle, obj.perception.getTime());
                    end
                case collision_detection.CollisionObject.Polygon
                    for os = obj.occupancy_set_polygon
                        os{1}.draw3D(axes_handle, obj.perception.getTime());
                    end
                otherwise
                    error('[OccupancyPrediction - draw3D] occupancyForm invalid');
            end
            hold off;
        end
        
        function [ dyn_obst ] =  getDynObstacles(obj)
            switch obj.occupancyForm
                case collision_detection.CollisionObject.Rect
                    dyn_obst = [obj.occupancy_set_rect{:}];
                case collision_detection.CollisionObject.Polygon
                    dyn_obst = [obj.occupancy_set_polygon{:}];
                otherwise
                    error('[OccupancyPrediction - getDynObstacles] occupancyForm invalid');
            end
        end
        
        function plotAtTime(obj, time_idx)
            switch obj.occupancyForm
                case collision_detection.CollisionObject.Rect
                    tmp = collision_detection.CollisionCheck.getDynamicObstaclesAtTime([obj.occupancy_set_rect{:}], time_idx);
                case collision_detection.CollisionObject.Polygon
                    tmp = collision_detection.CollisionCheck.getDynamicObstaclesAtTime([obj.occupancy_set_polygon{:}], time_idx);
                otherwise
                    error('[OccupancyPrediction - plotAtTime] occupancyForm invalid');
            end
            for i=tmp
                plot(i);
            end
        end
    end
    
    methods (Access=protected)
        function lane = getLaneWithID(obj, ID, lanelets)
            %GETLANEWITHID returns a lane from the lanelet array with the
            %laneID |ID|
            %params:
            %   ID          -   id of the lane which needs to be found
            %   lanelets    -   array of all existing lanes
            
            for j = 1:length(lanelets)
                if ID == lanelets{1,j}.id
                    lane = lanelets{1,j};
                    break;
                end
            end
        end
    end
    
    methods (Static)
        function s = farthestLongitudinalReachability(v_i, a_max, v_max, v_s, t)
            % FARTHESTLONGITUDINALREACHABILITY compute the maximum reachable distance within time t
            
            % accelerate as much as possible
            assert(v_s<v_max)
            % assert(v_i<=v_max); % why this assertion?
            % t = t1 + t2 + t3
            t1=0; % time interval v < v_s
            t2=0; % time interval v >= v_s and v < v_max
            t3=0; % time interval v >= v_max
            if (v_i >= v_s)
                t1 = 0; % jump to t2
            else
                t1 = min(t,(v_s-v_i)/a_max); % accelerate to v_s
            end
            if (v_i >= v_max)
                t2 = 0; % jump to t3
            else
                t2 = min((v_max^2-max(v_s^2,v_i^2))/2/a_max/v_s, t-t1); % accelerate to v_max
            end
            t3 = t-t1-t2;
            % distance travelled within t1, t2 and t3
            s1 = v_i*t1+1/2*a_max*t1^2;
            s2 = 1/(3*a_max*v_s)*((max(v_s^2,v_i^2)+2*a_max*v_s*t2)^(3/2)-max(v_s^3,v_i^3));
            s3 = v_max*t3;
            s = s1+s2+s3;
        end
        
        function s = closestLongitudinalReachability(v_i, a_max, t)
            % CLOSESTLONGITUDINALREACHABILITY compute the shortest possible distance within time t
            
            t1 = min(t,v_i/a_max);
            s = v_i*t1-1/2*a_max*t1^2;
        end
    end
end
