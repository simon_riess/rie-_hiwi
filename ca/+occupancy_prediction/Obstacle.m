classdef Obstacle < handle
    %OBSTACLEABSTRACT Describe static or dynamic obstacle over time
    %   The shape in workspace (R^2) of the obstacle must be of the same
    %   class (rectangle, polygon etc...) over the whole timerange
    %
    %   Warning: There is NO type safety in the storage of obstacles
    
    properties (SetAccess = protected)
        shape_type  %shape type of obstacle - either Rect or Polygon
        moving      %true if it is a dynamic obstacle, else false for static obstacle
    end
    
    properties (SetAccess = private)
        ts                  % sampling time
        time_point          % define the timestep index for each geometric boundary
        geometric_boundary  % a R^2 set of specified shape_type for each time interval
                            % at [time_point(idx), time_point(idx)+ts]
    end
    
    methods (Static)
        % there is NO type safety
        function obj = create_empty_dynamic_obstacle(shape_type, time_point, ts)
            % ts sampling time
            % time_point
            
            obj = occupancy_prediction.Obstacle(shape_type, true);
            obj.ts = ts;
            obj.time_point = time_point;
            obj.geometric_boundary = cell([1, numel(time_point)]);
        end
        
        function obj = create_dynamic_obstacle(shape_type, time_point, time_dependent_boundary)
            assert(numel(time_dependent_boundary) == numel(time_point));
            obj = occupancy_prediction.Obstacle(shape_type, true);
            obj.time_point = time_point;
            obj.geometric_boundary = time_dependent_boundary;
        end
        
        function obj = create_static_obstacle(shape_type, shape)
            obj = occupancy_prediction.Obstacle(shape_type, false);
            obj.geometric_boundary = {shape};
        end
    end
    
    methods
        function set_geometric_boundary_at_time(obj, idx, occupancy)
            assert(obj.moving);
            assert(idx<=size(obj.geometric_boundary,2));
            obj.geometric_boundary{idx} = occupancy;
            occupancy.linewidth = simulation.GlobalPlotProperties.LINEWIDTH_PREDICTION;
            occupancy.color = simulation.GlobalPlotProperties.COLOR_PREDICTION;
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function draw_time_interval(obj, axes_handle, t_start, t_stop)
            if obj.moving
                start = max(obj.time_point(1), t_start);
                stop = min(obj.time_point(end), t_stop);
                n = stop-start+1;
                idx = start-obj.time_point(1)+1;
                for i = idx:idx+n-1
                    obj.geometric_boundary{i}.draw(axes_handle);
                end
            end
        end
        
        function draw3D(obj, axes_handle, current_time)
            if obj.moving
                for i = 1:size(obj.geometric_boundary,2)
                    h = obj.ts*10;
                    switch obj.shape_type
                        case collision_detection.CollisionObject.Rect
                            obj.geometric_boundary{i}.drawAsBox((obj.time_point(i)-current_time)*h,h,[1 0 1]);
                        case collision_detection.CollisionObject.Polygon
                            obj.geometric_boundary{i}.draw3D();
                        otherwise
                             error('[Obstacle - draw3D] shape_type invalid');
                    end
                end
            end
        end
    end
    
    methods (Access = private)
        function obj = Obstacle(shape_type, moving)
            assert(islogical(moving));
            assert(isa(shape_type, 'collision_detection.CollisionObject'));
            obj.shape_type = shape_type;
            obj.moving = moving;
        end
        
        function draw(obj, axes_handle)
            if obj.moving
                for i = 1:size(obj.geometric_boundary,2)
                    plot(obj.geometric_boundary{i});
                end
            end
        end
    end
end