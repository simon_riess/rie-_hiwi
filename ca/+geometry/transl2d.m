function pts_out = transl2d(displacement, pts_in)
% TRANSL2D  tranlate points from frame_in to frame_out
%   pts_out = transl2d(displacement, pts_in) pts_in are shifted by
%   -displacement (minus!)

pts_out = [pts_in(1,:)-displacement(1); pts_in(2,:)-displacement(2)];

end
