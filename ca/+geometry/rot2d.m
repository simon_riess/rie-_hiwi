function pts_out = rot2d(theta, pts_in)
% ROT2D rotates a set of points of frame in to frame out
%    pts_out = rot2d(theta, pts_in) rotates pts_in clockwise by angle theta
% 

assert(size(pts_in,1)==2);
R = [cos(theta), -sin(theta); sin(theta), cos(theta)];
pts_out = R*pts_in;

end
