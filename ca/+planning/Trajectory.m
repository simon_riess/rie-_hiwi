classdef Trajectory < handle
    %TRAJECTORY describes the configuration (or state) of the system in SE2 
    %   over time.
    %
    %   The number of state variables may vary according to the dynamic of
    %   the modeled system.
    
    properties (SetAccess = protected)
        position
        velocity
        orientation
        yaw_rate
        ts
        t0
        distance % TODO
        
        num_samples
    end
    properties
        current_time = 0;
    end
    properties
        color = [1 0 0]
    end
    
    methods (Access = private)
        
        function obj = Trajectory(position,velocity,orientation,yaw_rate,t0,ts)
            % position 2xn 
            % velocity 2xn
            % orientation 1xn
            % yaw_rate 1xn
            % t0 time index of first element
            % ts sampling time in sec
            if ~isempty(yaw_rate)
                assert(size(position,2) == numel(yaw_rate));
            end
            if ~isempty(orientation)
                assert(size(position,2) == numel(orientation));
            end
            if ~isempty(velocity)
                assert(numel(position) == numel(velocity));
            end
            obj.position = position;
            obj.velocity = velocity;
            obj.orientation = orientation;
            obj.yaw_rate = yaw_rate;
            obj.t0 = t0;
            obj.ts = ts;
            obj.current_time = 0;
            obj.num_samples = size(position,2); 
            obj.calculate_distance_from_vertices();
            if isempty(orientation)
                obj.calculate_orientation_from_vertices()
            end
            if isempty(velocity)
                obj.calculate_velocity_from_vertices()
            end
            if isempty(yaw_rate)
                obj.calculate_yaw_rate_from_vertices()
            end
        end
    end
    
    methods
        
        function [out_of_bound, position, velocity, orientation, yaw_rate] ...
                = getStateAtTime(obj, time_idx)
            idx = time_idx - obj.t0 + 1;
            obj.current_time = time_idx;
            if idx>obj.num_samples
                out_of_bound = true; position = []; velocity = []; orientation = []; yaw_rate = [];
            else
                out_of_bound = false;
                position = obj.position(:,idx);
                velocity = obj.velocity(:,idx);
                orientation = obj.orientation(idx);
                yaw_rate = obj.yaw_rate(idx);
            end
        end
        
        function n = getLength(obj)
            n = obj.num_samples;
        end
        
        function writeToDOMNode(obj, doc, parent_node)
            traj_node = doc.createElement('Trajectory');
            parent_node.appendChild(traj_node);
            s = {'position','velocity','orientation','yaw_rate','t0','ts'};
            for k=1:numel(s)
                traj_node.setAttribute(s{k}, num2str(obj.(s{k})(:)'));
            end
        end
        
        function draw(obj, axes_handle)
            plot(axes_handle, obj.position(1,:), ...
                obj.position(2,:), 'g', 'LineWidth',1, ...
                'Color', obj.color);
        end
        
        function draw3D(obj, axes_handle)
            idx_start = round(obj.current_time/obj.ts)+1;
            idx_stop = idx_start+5/obj.ts;
            if idx_start > size(obj.position,2)
                idx_start = size(obj.position,2);
            end
            if idx_stop > size(obj.position,2)
                idx_stop = size(obj.position,2);
            end
            plot3(obj.position(1,idx_start:idx_stop), obj.position(2,idx_start:idx_stop), ...
                (0:idx_stop-idx_start)*obj.ts*obj.time_z_multiplier, ...
                'g', 'LineWidth',2);
        end
        
        % the time axis (z-axis) starts at time_offset (z=0), one timestep
        % equal h units along the z-axis. The trajectory starting from
        % time_start is plotted
        function draw3DO(obj, h, time_offset)
            sel = (time_offset-t0+1):size(obj.position,2);
            plot3(obj.position(1,sel), obj.position(2,sel), (0:numel(sel)-1)*h);
        end
             
        function recalculate_from_vertices(obj)
            obj.calculate_orientation_from_vertices();
            obj.calculate_velocity_from_vertices();
            obj.calculate_distance_from_vertices();
            obj.calculate_yaw_rate_from_vertices()
        end
        
        function plot(varargin)
            obj = varargin{1};
            obj.draw(gca);
        end
    end
    
    methods (Access = private)
        function calculate_orientation_from_vertices(obj)
            % assumes constant velocity ds = v dt
            if size(obj.position,2)<2
                return;
            end
            dxds = obj.position(:,2:end)-obj.position(:,1:end-1);
            orientation = [atan2(dxds(2,:), dxds(1,:)) 0];
            orientation(end) = orientation(end-1);
            obj.orientation = orientation;
        end
        
        function calculate_velocity_from_vertices(obj)
            % assumes constant velocity ds = v dt
            if size(obj.position,2)<2
                return;
            end
            velocity = 1/obj.ts*[obj.position(:,2:end)-obj.position(:,1:end-1) [0;0]];
            velocity(:,end) = velocity(:,end-1);
            obj.velocity = velocity;
        end
        
        function calculate_yaw_rate_from_vertices(obj)
            if size(obj.orientation,2)<2
                return;
            end
            obj.yaw_rate = [1/obj.ts*(obj.orientation(2:end)-obj.orientation(1:end-1)) 0];
            obj.yaw_rate(end) = obj.yaw_rate(end-1);
        end
        
        function calculate_distance_from_vertices(obj)
            dxds = obj.position(:,2:end)-obj.position(:,1:end-1);
            distance = [0 sqrt(dxds(1,:).^2+dxds(2,:).^2)];
            obj.distance = distance;
        end
    end
    
    % Factory methods
    methods (Static)
        % should do the same as 'createTrajectoryFromLaneVelProfile' but
        % faster
        function obj = mex_createTrajectoryFromLaneVelProfile(lane, ts, t0, velocity, distance_offset)
            % lane Lane
            % ts timestep
            % t0 time_index start
            % velocity at each timestep
            % distance_offset within a lane
            assert(size(velocity,1)==1);
            tn = numel(velocity);
            time_partition = ((1:tn)+(t0-1))*ts;
            traj = mex_path2trajectory(lane.distance, lane.center_vertices, time_partition, velocity, distance_offset);
            obj = planning.Trajectory(traj,[],[],[],t0,ts);
        end
        
        function obj = createTrajectoryFromPath(path_pos, t0, ts, velocity, distance_offset)
            % Convert a path to a trajectory using a velocity profile
            %
            % path_pos:         2xn position vertices
            % path_orientation: 1xn orientation 
            % t0:               time index of first point of trajectory
            % ts:               timestep in seconds
            % velocity:         piecewise constant velocity during each timestep
            % distance_offset:  start at trajectory at arc length offset of path_pos
            
            assert(distance_offset>=0);
            
            N = size(path_pos,2);
            arc_length = zeros(1,N);
            for i=2:N
                arc_length(i) = arc_length(i-1)+ norm(path_pos(:,i)-path_pos(:,i-1));
            end            
          
            cur_dst = distance_offset;
            seg_idx = 1;
            vel_idx = 1;
            traj_pos = zeros(2,0);
            out_of_path_bound = false;
            
            for vel_idx=1:numel(velocity+1)
                % find current segment
                while ~(arc_length(seg_idx) <= cur_dst && cur_dst <= arc_length(seg_idx+1))
                    seg_idx=seg_idx+1;
                    if seg_idx>=numel(arc_length)
                        out_of_path_bound = true;
                        break;
                    end
                end
                if out_of_path_bound
                    break;
                end
                r = (cur_dst-arc_length(seg_idx))/(arc_length(seg_idx+1)-arc_length(seg_idx));
                traj_pos(:,end+1) = (1-r)*path_pos(:,seg_idx)+r*path_pos(:,seg_idx+1);
                
                if vel_idx<=numel(velocity) % ugly
                    cur_dst = cur_dst+velocity(vel_idx)*ts;
                end
            end
            obj = planning.Trajectory(traj_pos,[],[],[],t0,ts);
        end
        
        function obj = createTrajectoryFromControlInput(u_steer, u_vel, ts)
            % Given the steering angle u_steer and velocity u_vel for
            % discretized time with timestep ts, calculate the trajectory
            %assert(false);
            
            assert(numel(u_steer) == numel(u_vel));
            assert(size(u_steer,1) == 1);
            
            N = numel(u_steer);
            theta = 0;
            
            position = zeros(2,N);
            position(:,1) = [0; 0];

            orientation = zeros(1,N);
            orientation(:,1) = theta;
            
            for i=2:N
                theta = mod(theta+u_vel(i-1)*u_steer(i-1)*ts,2*pi);
                Xd = cos(theta)*u_vel(i-1);
                Yd = sin(theta)*u_vel(i-1);
                position(:,i) = position(:,i-1)+ts*[Xd; Yd];
                orientation(i) = theta;
            end
            obj = planning.Trajectory(position,[],orientation,[],0,ts);
        end
        
        function obj = createTrajectoryFromTimePosition(points, ts, t0)
            obj = planning.Trajectory(position,[],[],[],t0,ts);
        end
        
        function trajectory = createTrajectoryFromDOMNode(node)
            s = {'position','velocity','orientation','yaw_rate','t0','ts'};
            tmp = str2num(node.getAttribute('position'));
            position = reshape(tmp,2,numel(tmp)/2);
            tmp = str2num(node.getAttribute('velocity'));
            velocity = reshape(tmp,2,numel(tmp)/2);
            orientation = str2num(node.getAttribute('orientation'));
            yaw_rate = str2num(node.getAttribute('yaw_rate'));
            t0  = str2num(node.getAttribute('t0'));
            ts  = str2num(node.getAttribute('ts'));
            trajectory = planning.Trajectory(position,velocity,orientation,yaw_rate,t0,ts);
        end
    end
end