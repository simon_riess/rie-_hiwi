classdef Rectangle < handle
    %RECTANGLE is a 2-D objects which can be used in intersection tests
    %
    %
    %   See also Circle
    
    %
    % v4 ---------------------- v1
    %    |                    |
    %    |                    |
    %    |        +--->       |  obj.width
    %    |        position    |
    %    |        orientation |
    % v3 ---------------------- v2
    %         obj.height
    %
    
    properties (SetAccess = private)
        width
        height
        orientation
        position
    end
    properties (SetAccess = private)
        radius
        vertices
    end
    properties
        color = [0 0 1];
        linewidth = 1;
    end
    
    methods
        function obj = Rectangle(width, height, orientation, position)
            obj.width = width;
            obj.height = height;
            obj.orientation = orientation;
            if size(position,2) == 2
                obj.position = position';
            else
                obj.position = position;
            end
            
            obj.radius = sqrt(width^2/4+height^2/4);
            
            w = obj.height/2;
            h = obj.width/2;
            vert = [w w -w -w; h -h -h h];
            vert = geometry.rot2d(obj.orientation, vert);
            vert = [vert(1,:) + obj.position(1); vert(2,:) + obj.position(2)];
            obj.vertices = vert;
        end
        
        function i = intersect(obj, rect)
            i = collision_detection.CollisionCheck.intersectRectRect(obj, rect);
        end
        
        function [pt dist idx edge] = nearestPointOnRectangleFromPt(obj, pt)
            pt_proj = [];
            dist = [];
            rd = [];
            for k=1:4
                [ pt_proj(:,k), dist(k), rd(k) ] = geometry.project_point_line_segment( obj.vertices(:,k), obj.vertices(:,mod(k,4)+1), pt );
            end
            [D, I] = sort(dist);
            if abs(dist(I(1))-dist(I(2)))<0.00001
                edge = false;
            else
                edge = true;
            end
            dist = dist(I(1));
            idx = I(1);
            pt = pt_proj(:,idx);
        end
        
        % OBSOLETE
        %function d = distance(obj, rect)
        %    
        %end
        
        function d = min_distance(obj, rect)
            d = max(0,norm(rect.position-obj.position)-...
                sqrt((rect.width/2)^2+(rect.height/2)^2)-...
                sqrt((obj.width/2)^2+(obj.height/2)^2));
        end
        
        % DEPR
        function vert = get_vertices(obj)
            warning('the use of get_vertices is deprecated')
            vert = obj.vertices;
        end
        
        function drawAsBox(obj, z, h, c)
            if nargin==2
                c = [1 0 0];
            end
            misc.plot_box(obj.height,obj.width,obj.position, obj.orientation,z,h,c);
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function writeToDOMNode(obj, doc, parent_node)
            node = doc.createElement('Rectangle');
            parent_node.appendChild(node);
            s = {'width', 'height', 'orientation', 'position'};
            for k=1:numel(s)
                node.setAttribute(s{k}, num2str(obj.(s{k})(:)'));
            end
        end
        
        function draw(obj, axes_handle)
            v = obj.vertices;
            v = [v v(:,1)];
            plot(v(1,:), v(2,:), 'Color', obj.color, 'Linewidth', obj.linewidth);
        end
    end
    
    methods (Static)
        function rect = createRectangleFromDOMNode(node)
            width = str2num(node.getAttribute('width'));
            height = str2num(node.getAttribute('height'));
            orientation = str2num(node.getAttribute('orientation'));
            tmp = str2num(node.getAttribute('position'));
            position = reshape(tmp,2,numel(tmp)/2);
            rect = collision_detection.Rectangle(width, height, orientation, position);
        end
        
        function rect = createFromAATwoPoints(region)
            % create rectangle of region given by
            % [min_x min_y max_x max_y]
            rect = collision_detection.Rectangle(region(4)-region(2), ...
                region(3)-region(1), 0, 0.5*(region([1,2])+region([3,4])));
        end
    end
end

