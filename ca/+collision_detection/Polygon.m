classdef Polygon < handle
    %POLYGON is a 2-D object
    %
    % v1--...--vN+3--vN+2
    % |              |
    % v2             vN+1
    % |              |
    % v3--v4-- ... --vN
    %
    
    properties(SetAccess = private)
        vertices    %array of points which define the polygon
                    %vertices = [x ; y], x = vector of x-coordinates of all
                    %points, y = vextor of y-coordinates of all points
    end
    properties
        color = [0 0 1];
        linewidth = 1;
    end
    
    methods
        function obj = Polygon(vertices)
            obj.vertices = vertices;
        end
        
        function setVertices(obj, vertices)
           %SETVERTICES changes all vertices to the new vertices
           % vertices - matrix with the new x/y Positions of the vertices
           obj.vertices = vertices;
        end
        
        function addVertice(obj, point)
           %ADDVERTICE adds a point to vertices
           % point - point which is added to vertices
           obj.vertices(1,end+1) = point(1);
           obj.vertices(2,end) = point(2);
        end
        
        function addVertices(obj, points)
           %ADDVERTICES adds matrix/points to vertices
           % points - points which are added to vertices
           obj.vertices = [obj.vertices,points];
        end
        
        function drawPDE(obj)
           %DRAW draws the polygon
           
           %get all x-coordinates
           x = obj.vertices(1,:);
           %get all y-coordinates
           y = obj.vertices(2,:);
           
           %draws a polygon with the points at coordinates x,y
           pdepoly(x,y);
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function draw(obj, axes_handle)
            %DRAW draws/plots the polygon
            
            x = obj.vertices(1,:);
            y = obj.vertices(2,:);
            
            %plot polygon
            hold on;
            if simulation.GlobalPlotProperties.POLYGON_TRANSPARENT
                patch(x,y,'white', 'EdgeColor', 'green', 'FaceAlpha', 0);
            else
                patch(x,y,'white', 'EdgeColor', 'green', 'FaceAlpha', 1);
            end
        end
        
        function draw3D(obj)
           %DRAW3D draws the polygon in 3D
           
           x = obj.vertices(1,:);
           y = obj.vertices(2,:);
           z = zeros(1,length(x));
           
           hold on;
           fill3(x,y,z,'green');
        end
    end
end