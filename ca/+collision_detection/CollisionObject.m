classdef CollisionObject
    %COLLISIONOBJECT class containing an enumeration about the type of the
    %occupancy representation
    %   occupancy representation can either be a rectangle (=Rect) or a
    %   polygon (Polygon)
    
    enumeration
        Rect, Polygon
    end
    
end