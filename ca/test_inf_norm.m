%include external folder for mathematical functions
addpath(genpath('external'));

clc; cla; clear

% timestep
dt = 0.15;
%open from manual xml
%s = simulation.Simulator.openFromXML('scenarios/two_lane_traffic2.xml');
%open from osm file
%trajectory within map
s = simulation.Simulator.openFromOSM('scenarios/1_mit_trajektorie.osm','');
%one file for the map, one file for the trajectories
%s = simulation.Simulator.openFromOSM('scenarios/JOSM_curvedLane.osm','scenarios/JOSM_curvedLane_CarTraj_2Vel.osm');

% add another obstacle
% s.map.addStaticRectangle(collision_detection.Rectangle(4,5,0, [45;-2.5]));

perception = simulation.Perception(s);

%%

cla;
plot(s);

%{
%manual import of trajectories of cars
%caclulate simulatedCar from center_vertices from lane
velocity_profile = 30-10*s.ts*(1:200); velocity_profile(velocity_profile<0) = 0; %braking
traj = planning.Trajectory.createTrajectoryFromPath(perception.getLanes{3}.center_vertices, 0, s.ts, velocity_profile,10);
sim_car = simulation.SimulatedCar(traj);
s.addDynamicObstacle(sim_car);
%}

%% step forward one timestep
% for i=1:100
% cla;
% s.step();
% clc
% hold on;
% plot(s);
% pause(1/10);
% end

%%

% % calculate predicted occupancy
% op = occupancy_prediction.OccupancyPrediction(perception, dt, collision_detection.CollisionObject.Polygon);
% %op = occupancy_prediction.OccupancyPrediction(perception, dt, collision_detection.CollisionObject.Rect);
% 
% op.set_prediction_horizon(30);
% op.update_prediction();
% plot(op)
% 
% %return cell-array with occupancies
% occupancy = op.getOccupancy();