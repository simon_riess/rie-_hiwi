classdef LaneNode < handle
    %LANENODE connects several lanes
    %
    %   A lane ends or starts in a LaneNode. Several lanes can end in
    %   LaneNode or start from one LaneNode.
    %
    %   See also Lane
    
    properties (SetAccess = private)
        lanesegments_end = {};  % Lanesegments ending in this node
        lanesegments_start = {}; % Lanesegment start from this node
        pos = [];
        id;
    end
    
    properties (Constant)
        NODE_LANE_OFFSET = 0.01;
    end
    
    methods
        function obj = LaneNode(pos, id)
            obj.pos = pos;
            obj.id = id;
        end
        
        function addLanesegmentEnd(obj, lanesegment)
            if norm(lanesegement.center_vertices(:,end)-pos) < obj.NODE_LANE_OFFSET
                error('LaneNode.addLanesegmentEnd: end of LaneSegment does not coincide with position of LaneNode')
            end
            obj.lanesegments_end{end} = lanesegment;
        end
        
        function addLanesegmentStart(obj, lanesegment)
            if norm(lanesegement.center_vertices(:,1)-pos) < obj.NODE_LANE_OFFSET
                error('LaneNode.addLanesegmentStart: start of LaneSegment does not coincide with position of LaneNode')
            end
            obj.lanesegments_start{end} = lanesegment;
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
    end
    
    methods (Access=private)
        function draw(obj, axes_handle)
            hold on;
            s = 0.2;
            tmp = [s*[1 0 -1 0]+obj.pos(1); ...
                s*[0 -1 0 1]+obj.pos(2);];
            fill(tmp(1,:), tmp(2,:), 'k');
            text(obj.pos(1), obj.pos(2), 0, '  node');
            hold off;
        end
    end
end