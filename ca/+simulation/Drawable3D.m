% % DEPRECATED
% 
% 
% % TODO: convert to abstract class
% % classdef (Abstract) Drawable < handle
% classdef Drawable3D < handle
%     %DRAWABLE Abstract classes that defines objects which can be plotted
%     %   Every object that might be plotted must be inherited from this
%     %   class
%     
%     properties (SetAccess=private)
%         cam_orientation3D
%         cam_position3D
%     end
%     
%     properties (Constant)
%         time_z_multiplier = 5 % one second equals 5 "meters" on z-axis
%     end
%     
%     methods (Abstract)
%         draw3D(obj, axes_handle)
%     end
%     
%     methods
%         function setCamera3D(obj, cam_orientation3D, cam_position3D)
%             obj.cam_orientation3D = cam_orientation3D;
%             obj.cam_position3D = cam_position3D;
%         end
%     end
%     
% end