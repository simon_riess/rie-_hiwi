classdef EgoCar < handle & simulation.SimulatedCar
    %EGOCAR describes the autonomous vehicle, the ego car
    
    methods
        function obj = EgoCar(trajectory)
            obj = obj@simulation.SimulatedCar(trajectory);
            obj.color = simulation.GlobalPlotProperties.COLOR_EGO_VEHICLE;
        end
                
        % the simulator executes the next step of the ego car and all other
        % dynamic objects, thereafter calls this update method
        function updatePlan(obj)
        end
        
        function step(obj)
            obj.time_idx = obj.time_idx+1;
            [out_of_bound, position, velocity, orientation, yaw_rate] ...
                = obj.trajectory.getStateAtTime(obj.time_idx);
            if out_of_bound
                disp('STATUS: arrived at end of trajectory');
            end
            obj.position = position;
            obj.orientation = orientation;
            obj.velocity = norm(velocity); % refactor all methods using this value
        end
        
        function writeToDOMNode(obj, doc, parent_node)
            car_node = doc.createElement('EgoCar');
            parent_node.appendChild(car_node);
            obj.trajectory.writeToDOMNode(doc, car_node);
        end
    end
    
    methods (Static)
        function car = createSimulatedCarFromDOMNode(node)
            traj_node = node.getElementsByTagName('Trajectory');
            if traj_node.size()~=1
                error('no trajectory associated with EgoCar');
            end
            traj = planning.Trajectory.createTrajectoryFromDOMNode(traj_node.item(0));
            car = simulation.EgoCar(traj);
        end
    end
end