classdef Car < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % state variables
        position
        orientation
        velocity
        laneID      %id of nearest lanes of obstacle
        roadDistance%after placint car on nearest lane, distance to this point
        obstacle    %obstacle wich represents the occupancy for this car
        compLanes   %composition lane array, defines occupancy        
        % 
        power_max
        a_max
        v_max
        v_sw
        v_x
        v_y
        width
        length
    end
    
    properties
        color = [0 0 0];
    end
    
    methods
        function c = Car(position,orientation,velocity,width,length)
            c.position = position;
            c.orientation = orientation;
            c.width = width;
            c.length = length;
            c.velocity = velocity;
            c.laneID = 0;
            c.roadDistance = 0;
            c.obstacle = [];
            c.compLanes = [];
        end
        
        function update(obj, position, orientation)
            obj.position = position;
            obj.orientation = orientation;
        end
        
        function draw(obj, axes_handle)
            import geometry.rot2d;
            import geometry.transl2d;
            
            p1 = [obj.length/2, obj.length/2, -obj.length/2, -obj.length/2, obj.length/2; obj.width/2, -obj.width/2, -obj.width/2, obj.width/2, obj.width/2];
            p2 = [0, obj.length/2; 0, 0];
            p1 = rot2d(obj.orientation,p1);
            p2 = rot2d(obj.orientation,p2);
            p1 = transl2d(-obj.position, p1);
            p2 = transl2d(-obj.position, p2);
            plot(axes_handle, p1(1,:), p1(2,:), 'Color', obj.color);
            plot(axes_handle, p2(1,:), p2(2,:), 'Color', obj.color);
        end
        
        function draw3D(obj, axes_handle)
            obj.draw(axes_handle);
        end
        
        function step(obj)
        end        
    end 
end