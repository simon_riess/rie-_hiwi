classdef SimulatedCar < simulation.Car
    %CARTEST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = protected)
        trajectory
        time_idx = 0
        %time = 0;
        ts;
    end
    
    methods
        function obj = SimulatedCar(trajectory)
            %obj = obj@simulation.Car( ...
            %    simulator.road.right_lane{lane}.center_vertices(:,1), ...
            %    simulator.road.right_lane{lane}.orientation(1), ...
            %    1.8, 4.2, simulator)
            obj = obj@simulation.Car([0,0], 0, 0, 1.8, 4.2);
            obj.trajectory = trajectory;
            obj.time_idx = -1;
            obj.color = simulation.GlobalPlotProperties.COLOR_DYNAMIC_OBSTACLES;
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function draw(obj, axes_handle)
            draw@simulation.Car(obj, axes_handle);
            if simulation.GlobalPlotProperties.SHOW_TRAJECTORIES
                obj.trajectory.color = obj.color;
                obj.trajectory.draw(axes_handle);
            end
        end
        
        function draw3D(obj, axes_handle)
            draw3D@simulation.Car(obj, axes_handle);
            obj.trajectory.draw3D(axes_handle);
        end
        
        function runUntil(obj, time_idx)
            while obj.time_idx<time_idx
                obj.step();
            end
        end
        
        function step(obj)
            obj.time_idx = obj.time_idx+1;
            [out_of_bound, position, velocity, orientation, yaw_rate] ...
                = obj.trajectory.getStateAtTime(obj.time_idx);
            if out_of_bound
                disp('STATUS: arrived at end of trajectory');
            end
            obj.position = position;
            obj.orientation = orientation;
            obj.velocity = norm(velocity); % refactor all methods using this value
        end
        
        function disp(obj)
            disp(['SimulatedCar (current time: ' num2str(obj.time_idx) ')'])
            disp(['  position: ' num2str(obj.position(1)) ' ' num2str(obj.position(2))])
            disp(['  orientation: ' num2str(obj.orientation/pi*180)])
        end

        function writeToDOMNode(obj, doc, parent_node)
            car_node = doc.createElement('SimulatedCar');
            parent_node.appendChild(car_node);
            obj.trajectory.writeToDOMNode(doc, car_node);
        end
    end
    
    methods (Static)
        function car = createSimulatedCarFromDOMNode(node)
            traj_node = node.getElementsByTagName('Trajectory');
            if traj_node.size()~=1
                error('no trajectory associated with SimulatedCar');
            end
            traj = planning.Trajectory.createTrajectoryFromDOMNode(traj_node.item(0));
            car = simulation.SimulatedCar(traj);
        end
        
        function car = createFromOSM(points, speed, ts)
            %CREATEFROMOSM creates a simulatedCar with given points
            %params:
            %   points  -   path of the car to create trajectory
            %   speed   -   if not empty, containing speed of the car
            %   ts      -   timestep in seconds
            
            if ~isempty(speed)
                velocity_profile = speed-10*ts*(1:200);
            else
                velocity_profile = 30-10*ts*(1:200); 
            end
            velocity_profile(velocity_profile<0) = 0; %braking
            %create trajectory
            traj = planning.Trajectory.createTrajectoryFromPath(points, 0, ts, velocity_profile, 10);
            %create simultedCar
            car = simulation.SimulatedCar(traj);
        end
    end
end