
% DEPRECATED

% TODO: convert to abstract class
% classdef (Abstract) Drawable < handle
% classdef Drawable < handle
%     %DRAWABLE Abstract classes that defines objects which can be plotted
%     %   Every object that might be plotted must be inherited from this
%     %   class
%     
%     properties (SetAccess=private)
%         cam_orientation
%         cam_position
%     end
%     
%     methods (Abstract)
%         draw(obj, axes_handle)
%     end
%     
%     methods
%         function setCamera(obj, cam_orientation, cam_position)
%             obj.cam_orientation = cam_orientation;
%             obj.cam_position = cam_position;
%         end
%     end
%     
% end