classdef Simulator < handle
    %SIMULATOR holds a reference to all static and dynamic objects
    %
    %   For each timestep Simulator calls each dynamic object to propagate
    %   in time and provides the ego car information about the environment
    %   created by a Perception object.
    %
    %   See also Lane, LaneNode, Perception.
    
    properties (Constant)
        time_horizont = 3 % only used for visualiziation
        FILEVERSION = 1000;
    end
    
    properties (SetAccess=private);
        ts                % timestep in sec
        map               % reference to the map of the simulation
        obstacles = {}    % (array of dynamic obstacles which will be called each timestep)
                          % = array of simulated cars
        time_idx          % current time index
        ego_car = [];     % reference to the ego car
    end
    
    properties
        description = []  % short description of the current simulations
        author = []       % author of the scenario
        date = []         % date of creation of scenario
    end
    
    methods
        function s = Simulator(map, ts)
            s.map = map;
            s.ts = ts;
            s.time_idx = 0;
        end
        
        function step(obj)
            obj.time_idx = obj.time_idx + 1;
            for k = 1:size(obj.obstacles,2)
                obj.obstacles{k}.runUntil(obj.time_idx);
            end
            if ~isempty(obj.ego_car)
                obj.ego_car.step();
            end
        end
        
        function addDynamicObstacle(obj, obstacle)
            obstacle.runUntil(obj.time_idx);
            obj.obstacles{end+1} = obstacle;
        end
        
        function addEgoCar(obj, ego_car)
            ego_car.runUntil(obj.time_idx);
            obj.ego_car = ego_car;
        end
        
        function plot(varargin)
            hold on
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function plot3(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw3D(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw3D(ax);
            end
        end
        
        function writeToXML(obj, filename)
            % check if file already exists
            [pathstr,name,ext] = fileparts(filename);
            if strcmp(pathstr,'')
                pathstr = pwd;
            end
            filename = fullfile(pathstr, [name ext]);
            if exist(filename) == 2
                warning('Simulator:writeToXML: File already exists. Skipping');
                return;
            end
            % create XML file
            doc = com.mathworks.xml.XMLUtils.createDocument...
                ('Simulation');
            rootNode = doc.getDocumentElement();
            rootNode.setAttribute('desc', obj.description);
            rootNode.setAttribute('author', obj.author);
            rootNode.setAttribute('date', obj.date);
            rootNode.setAttribute('fileversion', num2str(simulation.Simulator.FILEVERSION));
            % add ego car
            if ~isempty(obj.ego_car)
                obj.ego_car.writeToDOMNode(doc, rootNode);
            end
            % add dynamic obstacles
            if ~isempty(obj.obstacles)
                dyn_obs_node = doc.createElement('DynamicObstacles');
                for o=obj.obstacles
                    o{1}.writeToDOMNode(doc, dyn_obs_node);
                end
            end
            rootNode.appendChild(dyn_obs_node);
            % add map
            obj.map.writeToDOMNode(doc, rootNode);
            xmlwrite(filename, doc);
        end
        
        function disp(obj)
            disp(['Simulator (current time: ' num2str(obj.time_idx) ')'])
            disp(['  number of static obstacles: ' num2str(numel(obj.map.lane_boundary_rect))])
            disp(['  number of dynamic obstacles: ' num2str(size(obj.obstacles,2))])
            for i = 1:numel(obj.obstacles)
                disp(['  --obstacle ' num2str(i) ': position (' num2str(obj.obstacles{i}.position(1), '%0.2f') '/' ...
                    num2str(obj.obstacles{i}.position(2),'%0.2f') ') trajectory (' num2str(obj.obstacles{i}.time_idx) ' in ' ...
                    num2str(obj.obstacles{i}.trajectory.t0) '-' num2str(obj.obstacles{i}.trajectory.t0+obj.obstacles{i}.trajectory.getLength()-1) ')']);
            end
        end
    end
    
    methods (Access=protected)
        function draw(obj, axes_handle)
            hold on;
            if simulation.GlobalPlotProperties.SHOW_GRID
                grid on;
            else
                grid off;
            end
            if simulation.GlobalPlotProperties.SHOW_DYNAMIC_OBSTACLES
                for k = 1:size(obj.obstacles,2)
                    obj.obstacles{k}.draw(axes_handle);
                end
            end
            if simulation.GlobalPlotProperties.SHOW_EGO_VEHICLE
                %plot(axes_handle, obj.ego_car);
                plot(obj.ego_car);
            end
            if ~simulation.GlobalPlotProperties.SHOW_COORDINATE_AXES
                set(gca, 'Visible', 'off');
            end
            plot(obj.map);
        end
        
        function draw3D(obj, axes_handle)
            for k = 1:size(obj.obstacles,2)
                plot(axes_handle,obj.obstacles{k});
            end
            plot(axes_handle, obj.map);
        end
        
        function createSimulatedCars(obj, trajectory)
            %CREATESIMULATEDCARS creates simulated cars from imported
            %trajectories from OSM file
            %params:
            % trajectory -  array containing extracted trajectories for
            %               simulated Cars from OSM file; trajectories in row 1,
            %               corresponding velocity in row 2 if existing
            
            for k = 1:size(trajectory, 2)
                if size(trajectory,1) == 1
                    o = simulation.SimulatedCar.createFromOSM(trajectory{1,k}, [], obj.ts);
                else
                    o = simulation.SimulatedCar.createFromOSM(trajectory{1,k}, trajectory{2,k}, obj.ts);
                end
                obj.addDynamicObstacle(o);
            end
        end
    end
    
    methods (Static)
        function s = openFromXML(filename)
            [pathstr,name,ext] = fileparts(filename);
            if strcmp(pathstr,'')
                pathstr = pwd;
            end
            filename = fullfile(pathstr, [name ext]);
            if exist(filename) ~= 2
                error('File does not exist.');
            end
            
            try
                xmltree = xmlread(filename);
            catch
                error('Failed to read XML file %s.',filename);
            end
            root_node = xmltree.getDocumentElement();
            if ~strcmp(root_node.getNodeName, 'Simulation')
                error('Error: Simulation is not root node');
            end
            map_node = root_node.getElementsByTagName('Map');
            if map_node.getLength()~=1
                error('Error: More than one Map found');
            end
            map = simulation.Map.createMapFromDOMNode(map_node.item(0));
            s = simulation.Simulator(map, 0.01);
            dyn_obstacles_node = root_node.getElementsByTagName('DynamicObstacles');
            if dyn_obstacles_node.getLength()>1
                error('Error: More than DynamicObstacles element found');
            end
            if dyn_obstacles_node.getLength()==1
                sim_cars = dyn_obstacles_node.item(0).getElementsByTagName('SimulatedCar');
                for k=1:sim_cars.getLength()
                    o = simulation.SimulatedCar.createSimulatedCarFromDOMNode(sim_cars.item(k-1));
                    s.addDynamicObstacle(o);
                end
            end
            ego_car_node = root_node.getElementsByTagName('EgoCar');
            if ego_car_node.getLength() ~= 1
                warning('Cannot find EgoCar');
            else
            ego_car = simulation.EgoCar.createSimulatedCarFromDOMNode(ego_car_node.item(0));
            s.addEgoCar(ego_car);
            end
        end
        
        function s = openFromOSM(filenameMap, filenameDynObst)
            %OPENFROMOSM opens an osm file and extracts information for map
            %params:
            % filenameMap -     path to the OSM file containing the map (and
            %                   dynmaic obstacles)
            % filenameDynObst - path to the OSM file containing dynamic
            %                   obstacles
            
            if ~isempty(filenameMap)
                %create map
                %get lane boundaries
                [lanes, traj, starts] = xml_file_read(filenameMap, true);
                disp('Simulator')
                disp(size(starts))
                m = simulation.Map.createFromOSM(lanes, starts);
                %create simulator
                s = simulation.Simulator(m, 0.01);
                
                %create dynamic obstacles
                s.createSimulatedCars(traj);
            else
                error('Error: No Map file found');
            end
            if ~isempty(filenameDynObst)
                [~, traject] = xml_file_read(filenameDynObst, false);
                s.createSimulatedCars(traject);
            end
        end
    end
end