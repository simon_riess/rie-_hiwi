classdef GlobalPlotProperties < handle
    properties (Constant)
        SHOW_GRID = false;
        SHOW_DYNAMIC_OBSTACLES = true;
        SHOW_EGO_VEHICLE = true;
        SHOW_LANES = true;
        SHOW_TRAJECTORIES = true;
        SHOW_COORDINATE_AXES = false;
        SHOW_UPPERLOWERBOUND = false;
        
        PRINT_FIGURE = false;
        POLYGON_TRANSPARENT = false;
        
        COLOR_STATIC_OBSTACLES = [0 0 0];
        COLOR_DYNAMIC_OBSTACLES = [1 0 0];
        COLOR_PREDICTION = [1 0 0];
        COLOR_REACH_SAMPLE = [0 0 0];
        COLOR_EGO_VEHICLE = [0 0 1];
        
        LINEWIDTH_STATIC_OBSTACLES = 0.5;
        LINEWIDTH_DYNAMIC_OBSTACLES = 0.5;
        LINEWIDTH_PREDICTION = 0.5;
        LINEWIDTH_REACH_SAMPLE = 0.5;
    end
end