% TODO: convert to abstract class
% classdef (Abstract) Dynamic < handle
classdef Dynamic < handle
    %DYNAMIC Abstract classes that defines objects which act over time
    %   Every object that changes over time must be inherited from this
    %   class
    
    methods (Abstract)
        step(obj, timestep)
    end
end