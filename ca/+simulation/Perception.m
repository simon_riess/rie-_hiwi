classdef Perception < handle
    %Perception emulates the information that the EgoCar is provided for planning
    % 
    %   The information given by the Simulator is used to create
    %   information, that the EgoCar is allowed to use for planning
    
    properties
        simulator;
    end
    
    methods
        
        function obj = Perception(simulator)
            obj.simulator = simulator;
        end
        
        function cars = getDetectedCars(obj)
            cars = simulation.Car.empty;
            for o = obj.simulator.obstacles
                c = o{:};
                cars(end+1) = simulation.Car(c.position, c.orientation, c.velocity, c.width, c.length);
            end
        end
        
        % returns a cell array
        function obstacles = getStaticObstacles(obj)
            obstacles = obj.simulator.map.lane_boundary_rect;
        end
        
        % returns a cell array
        function lanes = getLanes(obj)
            lanes = obj.simulator.map.lane;
        end
        
        function time = getTime(obj)
            time = obj.simulator.time_idx;
        end
        
        function draw(obj,gca)
            tmp = obj.getStaticObstacles;
            for o = tmp
                o{1}.draw(gca);
            end
            tmp = obj.getLanes();
            for l = tmp
                l{1}.draw(gca);
            end
            tmp = obj.getDetectedCars()
            for c = tmp
                c.draw(gca);
            end
        end
    end
end