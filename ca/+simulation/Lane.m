classdef Lane < handle
    %LANE represents a single piece of a lane, a so called 'lanelet'
    %
    %   Two lanes are adjacent if they end and begin with the same point.
    %   Lane topologies can be build using the previousLanes and
    %   adjacentLanes arrays.
    %
    %   (not in this thesis:
    %   A lane always ends and starts from a LaneNode. Lane topologies can
    %   be built by using Lane connected by LaneNode
    %   See also LaneNode.)
    
    properties (SetAccess = private)
        center_vertices % vertices of the center positions of the lane
        orientation     % direction of travel at each point
        size            % number of vertices
        default_speed   % single value 
        width           % single value, width of road
        distance        % path length distance at each point (calc. from center_vertices)
        id              % a unique id
        left_border     % vertices of left lane border (calc. using width)
        right_border    % vertices of right lane border (calc. using width)
        obb             % oriented bounding box includeing the left and right lane border
        previousLanes   % id's of the previous lane if it exists
        adjacentLanes   % id's of the adjacent lanes if they exist
    end
    
    methods (Static)
        function id = add_id(init)
            % Creates unique ids
            persistent id_count;
            if nargin==1
                assert(isnumeric(init) && round(init) == init && numel(init)==1);
                id_count = int32(init);
            end
            if (numel(id_count)==0)
                id_count = 0;
            end
            id_count = id_count + 1;
            id = id_count;
        end
    end
    
    methods
        function obj = Lane(center_vertices, orientation, ...
                default_speed, width, leftBorder, rightBorder)
            
            assert(size(obj.center_vertices,2)==size(obj.orientation,2));
            %assert(size(obj.center_vertices,2)==size(obj.curvature,2));
            
            %to ensure that borders have identical number of points which
            %align perfectly (line through points is orthogonal to borders)
            if ~isempty(leftBorder) && ~isempty(rightBorder)
                %if no center_vertices exists - lane is created from
                %OSM/XML file, otherwise it is created for the prediction
                if isempty(center_vertices)
                    [leftBorder,rightBorder] = obj.alignBorderPoints(leftBorder, rightBorder, width);
                end
            end
            
            %if no center vertices calculate them
            if isempty(center_vertices)
                obj.createCenterVertices(leftBorder, rightBorder);
            else
                obj.center_vertices = center_vertices;
            end
            
            obj.orientation = orientation;
            %obj.curvature = curvature;
            obj.default_speed = default_speed;
            obj.width = width;
            obj.calcDistance(); % this is an approximation, TODO: replace by true distance 
            %if no borders exists calculate them
            if isempty(leftBorder)
                obj.calcBorders();
            else
                obj.left_border = leftBorder;
                obj.right_border = rightBorder;
            end
            obj.size = size(obj.center_vertices,2);
            obj.id = simulation.Lane.add_id();
            obj.calcOBB();
            obj.previousLanes = [];
            obj.adjacentLanes = [];
        end

        function [pt_out dist road_distance idx] = closest_point_on_lane(obj, pt)
            road_distance = -1;
            idx = knnsearch(obj.center_vertices(1:2,:)', pt');
            dist = 1;
            pt_out = [];
            if idx>1
                [pt_out dist rd] = geometry.project_point_line_segment(obj.center_vertices(1:2,idx-1), obj.center_vertices(1:2,idx), pt);
                rd = rd+obj.distance(idx-1);
            end
            if idx<size(obj.center_vertices,2)
                [pt2_out dist2 rd2] = geometry.project_point_line_segment(obj.center_vertices(1:2,idx), obj.center_vertices(1:2,idx+1), pt);
                if dist > dist2
                    dist = dist2;
                    pt_out = pt2_out;
                    rd = rd2+obj.distance(idx);
                end
            end
            road_distance = rd;
        end
        
        function [pt_out dist idx] = closest_point_on_laneBorder(obj, pt, border)
            road_distance = -1;
            idx = knnsearch(border(1:2,:)', pt');
            dist = 0;
            pt_out = [];
            if idx>1
                [pt_out dist rd] = geometry.project_point_line_segment(border(1:2,idx-1), border(1:2,idx), pt);
            end
            if idx<size(border,2)
                [pt2_out dist2 rd2] = geometry.project_point_line_segment(border(1:2,idx), border(1:2,idx+1), pt);
                if dist > dist2 || idx == 1
                    dist = dist2;
                    pt_out = pt2_out;
                end
            end
        end
        
        function [idx] = idx_distance(obj, distance)
            % obj.distance(idx) <= distance < obj.distance(idx+1), always?
            assert(distance>=obj.distance(1) && distance<=obj.distance(end));
            s_a = 1; s_b = numel(obj.distance);
            s_m = floor((s_a+s_b)/2);
            while s_b-s_a>1
                if distance>=obj.distance(s_m)
                    s_a = s_m;
                else
                    s_b = s_m;
                end
                s_m = floor((s_a+s_b)/2);
            end
            idx = s_a;
        end
        
        function [pos, posRight, posLeft, idx] = pos_interp(obj, distance)
            %POS_INTERP finds the interpolated position at path length distance
            % param: 
            %   distance - distance of the obstacle from the lane
            %   start
            % return: 
            %   pos         -   position of obstacle on lane center
            %   posRight    -   position of obstacle on right border of lane
            %   posLeft     -   position of obstacle on left border of lane
            %   idx         -   index of nearest vertice on lane center
            
            idx = obj.idx_distance(distance);
            if(idx<numel(obj.distance) && idx>0)
                rel = (distance-obj.distance(idx))/(obj.distance(idx+1)-obj.distance(idx));
                pos = (1-rel)*obj.center_vertices(:,idx)+rel*obj.center_vertices(:,idx+1);
                %for Polygon: calculate poinst on right and left border
                posRight = (1-rel)*obj.right_border(:,idx)+rel*obj.right_border(:,idx+1);
                posLeft = (1-rel)*obj.left_border(:,idx)+rel*obj.left_border(:,idx+1);
            else
                assert(false);         
            end
        end
        
        function calcOBB(obj)
            obox = orientedBox([obj.left_border obj.right_border]');
            obj.obb = collision_detection.Rectangle(obox(4),obox(3), obox(5)/180*pi, [obox(1); obox(2)]);
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                obj = varargin{2};
                ax = varargin{1};
                obj.draw(ax);
            end
        end
        
        function writeToDOMNode(obj, doc, parent_node)
            node = doc.createElement('Lane');
            parent_node.appendChild(node);
            s = {'center_vertices', 'default_speed', 'orientation', 'width'};
            for k=1:numel(s)
                node.setAttribute(s{k}, num2str(obj.(s{k})(:)'));
            end
        end
        
        function addPreviousLaneID(obj, prevLane)
            obj.previousLanes(1,end+1) = prevLane;
        end
        
        function addAdjacentLaneID(obj, adjLane)
            obj.adjacentLanes(1,end+1) = adjLane;
        end
        
        function setPreviousLanes(obj, prevLanes)
            obj.previousLanes = prevLanes;
        end
        
        function setAdjacentLanes(obj, adjLanes)
            obj.adjacentLanes = adjLanes;
        end
    end
    
    methods (Access = private)
        function draw(obj, axes_handle)
            plot(obj.center_vertices(1,:), obj.center_vertices(2,:),'g:');
            plot(obj.left_border(1,:), obj.left_border(2,:),'k-');
            plot(obj.right_border(1,:), obj.right_border(2,:),'k-');
            %plot upper and lower bound
            if simulation.GlobalPlotProperties.SHOW_UPPERLOWERBOUND
                plot([obj.left_border(1,1), obj.right_border(1,1)], ...
                    [obj.left_border(2,1), obj.right_border(2,1)],'k-');
                plot([obj.left_border(1,end), obj.right_border(1,end)], ...
                    [obj.left_border(2,end), obj.right_border(2,end)],'k-');
            end
            %set description: lane
            obj.obb.color = [0.7 0.7 0.5];
            text(obj.obb.position(1), obj.obb.position(2), 0, 'lane'); 
            %plot(axes_handle, obj.obb);
        end
        
        function calcDistance(obj)
            % piecewise linear approximation between each point
            N = size(obj.center_vertices,2);
            obj.distance = zeros(1,N);
            obj.distance(1) = 0;
            for i=2:N
                obj.distance(i) = obj.distance(i-1)+ ... 
                    norm(obj.center_vertices(:,i)-obj.center_vertices(:,i-1));
            end
        end
        function calcBorders(obj)
            shift = 0.3;
            obj.left_border = simulation.Lane.borderCurve(obj.center_vertices, ...
                obj.orientation, -(obj.width/2-shift));
            obj.right_border = simulation.Lane.borderCurve(obj.center_vertices, ...
                obj.orientation, (obj.width/2-shift));
        end
        
        function [lBorder, rBorder] = alignBorderPoints(obj, leftBorder, rightBorder, width)
           %ALIGNBORDERPOINTS adds point to left/right border in order to 
           % return two borders with identical number of points that align
           % perfectly (line through points is orthogonal to borders)
           %params:
           % leftBorder  - left border
           % rightBorder - right border
           %return:
           % lBorder = new calculated left border
           % rBorder = new calculated right border
           
           %define function insertInMatrix for inserting a vector into a
           %matrix
           insertInMatrix = @(e,m,n)cat(2,m(:,1:n-1),e,m(:,n:end));
           
           %mirror points from left to right and from right to left
           rBorder = rightBorder;
           lBorder = leftBorder;
           
           %mirror every point except the first and last points
           rightBorder(:,1) = [];
           rightBorder(:,end) = [];
           leftBorder(:,1) = [];
           leftBorder(:,end) = [];
           
           %from left to right
           for point = leftBorder
                %calculate point
                [pt_out, dist, idx] = obj.closest_point_on_laneBorder(point, rBorder);
                if ~isnan(pt_out(1)) && ~isnan(pt_out(2))
                    %find correct index to insert point
                    %get direction of lane
                    if idx ~= 1
                        if rBorder(1,idx-1) < rBorder(1,idx)
                            rightDir = true;
                        else
                            rightDir = false;
                        end
                    else
                        if rBorder(1,idx) < rBorder(1,idx+1)
                            rightDir = true;
                        else
                            rightDir = false;
                        end
                    end
                    %add point to rBorder
                    if rightDir
                        if pt_out(1) > rBorder(1,idx)
                            rBorder = insertInMatrix(pt_out, rBorder, idx+1);
                        else
                            rBorder = insertInMatrix(pt_out, rBorder, idx);
                        end
                    else
                        if pt_out(1) < rBorder(1,idx)
                            rBorder = insertInMatrix(pt_out, rBorder, idx+1);
                        else
                            rBorder = insertInMatrix(pt_out, rBorder, idx);
                        end
                    end
                else
                   %delete point from lBorder
                   k = find(lBorder(1,:) == point(1));
                   lBorder(:,k) = [];
                end
           end
           
           %from right to left
           for point = rightBorder
               %calculate point
               [pt_out, dist, idx] = obj.closest_point_on_laneBorder(point, lBorder);
               if ~isnan(pt_out(1)) && ~isnan(pt_out(2))
                    %find correct index to insert point
                    %get direction of lane
                    if idx ~= 1
                        if lBorder(1,idx-1) < lBorder(1,idx)
                            rightDir = true;
                        else
                            rightDir = false;
                        end
                    else
                        if lBorder(1,idx) < lBorder(1,idx+1)
                            rightDir = true;
                        else
                            rightDir = false;
                        end
                    end
                    %add point to lBorder
                    if rightDir
                        if pt_out(1) > lBorder(1,idx)
                            lBorder = insertInMatrix(pt_out, lBorder, idx+1);
                        else
                            lBorder = insertInMatrix(pt_out, lBorder, idx);
                        end
                    else
                        if pt_out(1) < lBorder(1,idx)
                            lBorder = insertInMatrix(pt_out, lBorder, idx+1);
                        else
                            lBorder = insertInMatrix(pt_out, lBorder, idx);
                        end
                    end
               else
                   %delete element from rBorder
                   k = find(rBorder(1,:) == point(1));
                   rBorder(:,k) = [];
               end
           end
           
           %eliminate points that are too close
           len = length(lBorder)-1;
           k = 1;
           while k < len
               if abs(lBorder(1,k+1) - lBorder(1,k)) < 0.01
                   lBorder(:,k+1) = [];
                   rBorder(:,k+1) = [];
                   len = len-1;
               end
               k = k+1;
           end
           len = length(rBorder)-1;
           k = 1;
           while k < len
               if abs(rBorder(1,k+1) - rBorder(1,k)) < 0.01
                   rBorder(:,k+1) = [];
                   lBorder(:,k+1) = [];
                   len = len-1;
               end
               k = k+1;
           end             
        end
        
        function createCenterVertices(obj, leftBorder, rightBorder)
           %CREATECENTERVERTICES creates center vertices from |leftBorder| 
            %and |rightBorder|
           
           assert(length(leftBorder)==length(rightBorder));
           obj.center_vertices =zeros(2,length(leftBorder));
           for k = 1:length(leftBorder)
               %calculate x and y values seperately in order to minimize
               %error (if not seperately - values in left/rightBorder will
               %be rounded to same *10^x -> error if values differ minimal,
               %e.g. (7.1008e+5;5.3585e+6) ->rounded: (0.7101e+6;5.3585e+6)
               %and  (7.1009e+5;5.3585e+6) ->rounded: (0.7101e+6;5.3585e+6)
               obj.center_vertices(1,k) = 0.5*(leftBorder(1,k)+rightBorder(1,k));
               obj.center_vertices(2,k) = 0.5*(leftBorder(2,k)+rightBorder(2,k));
           end
        end
    end
    
    methods (Static)
        function curve = borderCurve(center_vertices, orientation, distance)
            curve = center_vertices+[sin(orientation)*distance; ...
                -cos(orientation)*distance];
        end
        
        function lane = createLaneFromDOMNode(node)
            tmp = str2num(node.getAttribute('center_vertices'));
            center_vertices = reshape(tmp,2,numel(tmp)/2);
            default_speed = str2num(node.getAttribute('center_vertices'));
            orientation = str2num(node.getAttribute('orientation'));
            width = str2num(node.getAttribute('width'));
            lane = simulation.Lane(center_vertices, orientation, default_speed, width, {}, {});
        end
        
        function l = createFromOSM(leftBorder, rightBorder)
            %CREATEFROMOSM creates lane from |leftBorder| and |rightBorder|
            
            %if the points of the borders are in opposite directions ->
            %flip the border which goes in the wrong direction (= not in
            %driving direction)
            if leftBorder(1,1) < leftBorder(1,end) && rightBorder(1,1) > rightBorder(1,end)
                rightBorder = fliplr(rightBorder);
            elseif leftBorder(1,1) > leftBorder(1,end) && rightBorder(1,1) < rightBorder(1,end)
                leftBorder = fliplr(leftBorder);
            end
            %if points on left and right border go in opposite driving
            %direction -> flip borders
            if leftBorder(2,1) < rightBorder(2,1) && leftBorder(1,1) < leftBorder(1,2)
                leftBorder = fliplr(leftBorder);
                rightBorder = fliplr(rightBorder);
            elseif leftBorder(2,1) > rightBorder(2,1) && leftBorder(1,1) > leftBorder(1,2)
                leftBorder = fliplr(leftBorder);
                rightBorder = fliplr(rightBorder);
            end
            
            %create new lane
            def_speed = 80;
            orient = ones(1,length(leftBorder));
            w = sqrt((leftBorder(1) - rightBorder(1))^2 + ...
                (leftBorder(2) - rightBorder(2))^2);
            l = simulation.Lane({}, orient, def_speed, w, leftBorder, rightBorder);
        end
    end
end