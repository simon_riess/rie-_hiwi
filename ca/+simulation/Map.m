classdef Map < handle
    %MAP holds references to the road network topology and static obstacles
    %   as e.g border of lane.
    %
    %
    %   See also Lane, LaneNode
    
    properties (SetAccess = protected)
        lane = {}
        lane_node = {}
        lane_boundary_rect = {} % rectangle approximation of lane boundary for collision check
        start_node = {}
    end
    
    methods
        function obj = Map(varargin)
            if nargin==4
                obj.lane = varargin{1};
                obj.lane_node = varargin{2};
                obj.lane_boundary_rect = varargin{3};
                obj.start_node = varargin{4};
            else
                obj.lane = {};
                obj.lane_node = {};
                obj.lane_boundary_rect = {};
            end
        end
        
        function plot(varargin)
            if nargin == 1
                obj = varargin{1};
                obj.draw(gca);
            elseif nargin == 2
                ax = varargin{1};
                obj = varargin{2};
                obj.draw(ax);
            end
        end
        
        function addStaticRectangle(obj, rect)
            obj.lane_boundary_rect{end+1} = rect;
            obj.lane_boundary_rect{end}.color = simulation.GlobalPlotProperties.COLOR_STATIC_OBSTACLES;
            obj.lane_boundary_rect{end}.linewidth = simulation.GlobalPlotProperties.LINEWIDTH_STATIC_OBSTACLES;
        end
        
        function writeToDOMNode(obj, doc, parent_node)
            map_node = doc.createElement('Map');
            parent_node.appendChild(map_node);
            lanetop_node = doc.createElement('LaneTopolgy');
            map_node.appendChild(lanetop_node);
            for l=obj.lane
                l{1}.writeToDOMNode(doc, lanetop_node);
            end
            obs_node = doc.createElement('StaticObstacles');
            map_node.appendChild(obs_node);
            for l=obj.lane_boundary_rect
                l{1}.writeToDOMNode(doc, obs_node);
            end
        end
        
        function disp(obj)
            disp(['Map']);
            disp(['  number of static obstacles: ' num2str(numel(obj.lane_boundary_rect))]);
            disp(['  number of lanes: ' num2str(numel(obj.lane))]);
            disp(['  number of lane nodes: ' num2str(numel(obj.lane_node))]);
        end 
    end
    
    methods (Access = private)
        % TODO: replace by general lanes/lane_nodes representation
        function draw(obj, axes_handle)
            for k=1:size(obj.lane,2)
                if simulation.GlobalPlotProperties.SHOW_LANES
                    plot(obj.lane{k});
                end
            end
            for k=obj.lane_boundary_rect
                plot(k{1})
            end
            disp(size(obj.start_node))
            for k=1:size(obj.start_node,2)
                disp('X-Koordinate')
                disp(obj.start_node{k}(1))
                disp('Y-Koordinate')
                disp(obj.start_node{k}(2))
                plot(obj.start_node{k}(1),obj.start_node{k}(2), 'r*');
                disp('Punkt vorhanden')
            end
            axis equal;
        end
    end
    
    methods (Static)
        function map = createMapFromDOMNode(node)
            lt_node = node.getElementsByTagName('LaneTopolgy');
            if lt_node.size()~=1
                error('Error: Several LaneTopolgy elements found');
            end
            lt_node = lt_node.item(0);
            lane_nodes = lt_node.getElementsByTagName('Lane');
            lane = {};
            %disp([num2str(lane_nodes.getLength()) ' lanes found']);
            for i=1:lane_nodes.getLength()
                lane{i} = simulation.Lane.createLaneFromDOMNode(lane_nodes.item(i-1));
            end
            
            so_node = node.getElementsByTagName('StaticObstacles');
            if so_node.size()~=1
                error('Error: Several StaticObstacles elements found');
            end
            so_node = so_node.item(0);
            rect_nodes = so_node.getElementsByTagName('Rectangle');
            
            map = simulation.Map(lane, {}, {});
            for i=1:rect_nodes.getLength()
                map.addStaticRectangle(collision_detection.Rectangle.createRectangleFromDOMNode(rect_nodes.item(i-1)));
                %lane_boundary_rect{i} = collision_detection.Rectangle.createRectangleFromDOMNode(rect_nodes.item(i-1));
                %lane_boundary_rect{i}.color = simulation.GlobalPlotProperties.COLOR_STATIC_OBSTACLES;
                %lane_boundary_rect{i}.linewidth = simulation.GlobalPlotProperties.LINEWIDTH_STATIC_OBSTACLES;
            end            
        end
        
        function map = createMapOfShortRoadsegment(x_start, y_start, theta_start, kappa_start, v_start, ...
                length, d_s, num_left_lane, num_right_lane, create_random)
            % CREATE_ROAD_SEGMENT  Create vertices of a road
            %  x_start: x coordinate of starting vertex
            %  y_start: y coordinate of starting vertex
            %  theta_start: angle between x_axes of woorld coordinates
            %               and direction of road
            %  kappa_start: curvature at the starting vertex
            %  v_start: velocity at the starting vertex, TODO: remove
            %  length: length in meters
            %  d_s: discretization length
            %
            %  road trajectories a created along euler spirals
            
            KAPPA_MAX = 0.004;
            LANE_WIDTH = 3.5;
            if create_random
                random_value = rand;
            else
                random_value = 0.5;
            end
            
            d_t = d_s/v_start;
            T = length/v_start;
            t = 0:d_t:T;
            N = size(t,2);
            
            p = ones(1,4);
            p(1) = v_start;
            p(3) = 0;
            p(2) = kappa_start;
            if theta_start<pi/180*30 || theta_start>pi/180*330
                p(4) = (KAPPA_MAX*(2*random_value-1)-kappa_start)/T;
            elseif theta_start>=pi/180*30 && theta_start<=pi
                % turn right
                p(4) = -(kappa_start+KAPPA_MAX)/T*random_value;
            else
                % turn left
                p(4) = (KAPPA_MAX-kappa_start)/T*random_value;
            end
            v = p(1)+p(3)*t;
            kappa = p(2)+p(4)*t;
            theta = theta_start;
            simulation.Lane.add_id(0);
            
            % create data structure of the road
            position = zeros(2,N);
            position(:,1) = [x_start; y_start];
            distance = zeros(1,N);
            distance(:,1) = 0;
            orientation = zeros(1,N);
            orientation(:,1) = theta;
            lane = {};
            
            for i=2:N
                Xd = cos(theta)*v(i-1);
                Yd = sin(theta)*v(i-1);
                theta = mod(theta+v(i-1)*kappa(i-1)*d_t,2*pi);
                position(:,i) = position(:,i-1)+d_t*[Xd; Yd];
                orientation(i) = theta;
                distance(:,i) = (i-1)*d_s;
            end
            for k=1:num_right_lane
                lane{k} = simulation.Lane( ...
                    simulation.Lane.borderCurve(position, orientation, (0.5+k-1)*4.8), ...
                    orientation, 30, 4.8);
            end
            for k=1:num_left_lane
                lane{k+num_right_lane} = simulation.Lane( ...
                    fliplr(simulation.Lane.borderCurve(position, orientation, -(0.5+k-1)*4.8)), ...
                    fliplr(mod(orientation-pi,2*pi)), 30, 4.8);
            end
            tmp_lane = simulation.Lane( ...
                simulation.Lane.borderCurve(position, orientation, (0.5+num_right_lane)*4.8), ...
                orientation, 30, 4.8);
            lane_boundary_rect = make_obstacle_from_lane( tmp_lane, 4.8, 10 );
            tmp_lane = simulation.Lane( ...
                fliplr(simulation.Lane.borderCurve(position, orientation, -(0.5+num_left_lane)*4.8)), ...
                fliplr(mod(orientation-pi,2*pi)), 30, 4.8);
            tmp = make_obstacle_from_lane( tmp_lane, 4.8, 10 );
            lane_boundary_rect = {lane_boundary_rect{:} tmp{:}};
            map = simulation.Map(lane, {}, lane_boundary_rect);
        end
        
        function m = createFromOSM(lanes, starts)
            %CREATEFROMOSM creates a map from file with |filename|
            %params:
            %   lanes  -    array containing left and right borders of lanes
            %create lanes
            l = cell(1,length(lanes));
            for k = 1:length(lanes)
                %lanes with (left,right) - border
                l{k} = simulation.Lane.createFromOSM(lanes{k}{1}, lanes{k}{2});
            end
            
            %add previous / adjacent lanes if existing
            for k = 1:length(l)
                for j = 1:length(l)
                    if k ~= j
                        if l{1,k}.left_border(:,end) == l{1,j}.left_border(:,1)
                            l{1,k}.addAdjacentLaneID(l{1,j}.id);
                            l{1,j}.addPreviousLaneID(l{1,k}.id);
                        end
                    end
                end
            end
            
            %create map
            m = simulation.Map(l, {}, {}, starts);
        end
    end
end